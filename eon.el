;;; eon.el --- generating synthetic LLM dataset  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  David O'Toole

;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Keywords: tools, ai

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; The Software is provided "as is", without warranty of any kind,
;; express or implied, including but not limited to the warranties of
;; merchantability, fitness for a particular purpose and
;; noninfringement. In no event shall the authors or copyright holders
;; be liable for any claim, damages or other liability, whether in an
;; action of contract, tort or otherwise, arising from, out of or in
;; connection with the Software or the use or other dealings in the
;; Software.

;;; A note on contributed material:

;; Several functions in this file relating to context-free grammars
;; are adapted from code by Peter Norvig from his book "Paradigms of
;; Artificial Intelligence Programming". You can find more information
;; on Norvig's book at his website: http://www.norvig.com/paip.html
;; The full license for the PAIP code can be found at norvig.com:
;; http://www.norvig.com/license.html

;;; Commentary:

;; This file generates a synthetic dataset designed for fine-tuning a
;; large language model to execute a subset of Scheme. The companion
;; file "ion.el" generates an English conversation dataset between the
;; user and the assistant which leverages the first training.

;;; Code:

(eval-when-compile (require 'cl))
(require 'cl-lib)
(require 'json)
(require 'sie)

(defvar eon-alphabet '(a b c d e f g h i j k l m n o p q r s t u v w x y z))

(defvar eon-primitives ())

(setf eon-primitives
      '(% * + - / 1+ 1- < <= = > >= string=? zero? cons play-bell-sound
          scheme-version string-append emacs! add-task add-note
          append apply vector? vector-ref vector-set! array? assoc assq print display
          eq? equal? not atom? bound? pair? string? car cdr length reverse sequence?
          even? odd? integer?))

(defalias 'string-append 'concat)
(defalias 'even? 'evenp)
(defalias 'odd? 'oddp)
(defalias 'eq? 'eq)
(defalias 'equal? 'equal)
(defalias 'display 'print)
(defalias 'sequence? 'sequencep)
(defalias 'integer? 'integerp)
(defalias 'string? 'stringp)
(defalias 'vector? 'vectorp)
(defalias 'zero? 'zerop)
(defalias 'vector-ref 'aref)
(defalias 'vector-set! 'aset)
(defalias 'atom? 'atom)
(defalias 'array? 'arrayp)
(defalias 'pair? 'consp)
(defalias 'bound? 'boundp)
(defalias 'string=? 'string=)

(defalias 'eon:even? 'eon:evenp)
(defalias 'eon:odd? 'eon:oddp)
(defalias 'eon:eq? 'eon:eq)
(defalias 'eon:equal? 'eon:equal)
(defalias 'eon:display 'eon:print)
(defalias 'eon:sequence? 'eon:sequencep)
(defalias 'eon:integer? 'eon:integerp)
(defalias 'eon:string? 'eon:stringp)
(defalias 'eon:vector? 'eon:vectorp)
(defalias 'eon:zero? 'eon:zerop)
(defalias 'eon:vector-ref 'eon:aref)
(defalias 'eon:vector-set! 'eon:aset)
(defalias 'eon:atom? 'eon:atom)
(defalias 'eon:array? 'eon:arrayp)
(defalias 'eon:pair? 'eon:consp)
(defalias 'eon:bound? 'eon:boundp)
(defalias 'eon:string=? 'eon:string=)

(defvar eon|error-count 0)
(defvar eon|generations 88)
(defvar eon|width 3)
(defvar eon|depth 3)
(defvar eon|max-width 3)
(defvar eon|max-depth 4)
(defvar eon|num-scenarios 24)

(setf eon|generations 2048)
(setf eon|width 4)
(setf eon|depth 4)
(setf eon|max-width 3)
(setf eon|max-depth 5)
(setf eon|num-scenarios 1024)

(defun eon|depth-bottom-p () (zerop eon|depth))

(cl-defmacro eon/deeper (&body body)
  (declare (indent defun))
  `(let ((eon|depth (- eon|depth 1)))
     ,@body))

(defmacro eon/randomly (&rest forms)
  `(eval (eon!choose ',forms)))

;;; Grammars

;; Generate random sentences from context-free grammars, and then
;; interpret them how you want. This section's code is adapted from
;; Norvig's PAIP, see notice at the top of this file.

(defvar eon|grammar nil
  "The current context-free grammar used for sentence generation.
This is an association list of the form:

    ((VARIABLE . EXPANSIONS)
     (VARIABLE . EXPANSIONS)
     ...)

Where EXPANSIONS is a list of alternatives, each of which may be
either (1) single symbols or (2) a list of symbols, representing
concatenation.")

(defun eon!variable ()
  (eon!choose eon-alphabet))

(setf eon|grammar
      '((<math> eon:+ eon:- eon:* eon:/
                eon:% 
                (+ <math> <math>)
                (- <math> <math>)
                (* <math> <math>)
                (/ <math> <math>)
                eon!small-integer)
        (<strings> (vector-ref eon!string eon!small-integer)
                   (string? eon!atom))
        (<lists> eon:append eon:cons)
        (<definitions>
         [progv ((f (gensym)))
             `(begin
                (define (,f x) (+ eon!small-integer x))
                (,f eon!small-integer))]
         [progv ((f (gensym)))
             `(begin
                (define (,f x y) (* y (+ eon!small-integer x)))
                (,f eon!small-integer eon!small-integer))]
         [progv ((v (gensym)))
             `(begin
                (define ,v eon!small-integer)
                (+ 1 ,v))])))

(defun eon-start-symbols ()
  (mapcar #'car eon|grammar))

(cl-defmacro eon/with-grammar (grammar &body body)
  (declare (indent defun))
  `(let ((eon|grammar ,grammar))
     ,@body))

(defun eon-one-of (set)
  (nth (random (length set)) set))

(defun eon-left-hand-side (rule)
  (first rule))

(defun eon-right-hand-side (rule)
  (rest rule))

(defun eon-non-terminal-p (sym)
  (cond ((consp sym) t)
        ((symbolp sym)
         (string-equal "<" (substring (symbol-name sym) 0 1)))
        (t nil)))

(cl-defun eon-expansions (variable &optional (terminals-only-p (zerop eon|depth)))
  (let ((expansions (eon-right-hand-side (assoc variable eon|grammar))))
    (if terminals-only-p
        (remove-if #'eon-non-terminal-p expansions)
      expansions)))

(defun eon-progv-p (form)
  (and (vectorp form)
       (eq 'progv (aref form 0))))

(cl-defun eon-process-progv (phrase)
  (cl-destructuring-bind (key bindings &rest body)
      (coerce phrase 'list)
    (let ((variables (mapcar #'car bindings))
          (values (mapcar #'eval (mapcar #'car (mapcar #'cdr bindings)))))
      (let ((transformed-body
             (eon-transform-tree #'eon-non-terminal-p
                                 #'eon-generate-phrase
                                 body)))
        (eval `(cl-progv ',variables ',values
                 ,@transformed-body))))))

(cl-defun eon-generate-phrase (&optional (phrase '<start>))
  "Generate a random phrase using the grammar in `eon|grammar'."
  (let ((result (cond ((listp phrase)
	               (mapcar #'eon-generate-phrase phrase))
                      ((eon-progv-p phrase)
                       (eon-process-progv phrase))
	              ((eon-expansions phrase)
	               (eon/deeper (eon-generate-phrase
                                    (eon-one-of
                                     (eon-expansions phrase)))))
	              (t phrase))))
    result))

(defun eon-transform-tree (tester transformer tree)
  (cond ((consp tree)
	 ;; it's a cons. process the two subtrees.
	 (cl-destructuring-bind (left . right) tree
	   (cons
	    ;; process left subtree.
	    (if (funcall tester left)
		(funcall transformer left)
		;; nothing to transform here. move on down the left side.
		(if (consp left)
		    (eon-transform-tree tester transformer left)
		    left))
	    ;; process right subtree.
	    (eon-transform-tree tester transformer right))))
	;; it's not a cons. test it.
	((funcall tester tree)
	 (funcall transformer tree))
	;; it failed the test. leave it alone.
	(t tree)))

(defun eon-expand-generators (tree)
  (eon-transform-tree #'(lambda (subtree)
                          (when (symbolp subtree)
                            (when (< 4 (length (symbol-name subtree)))
                              (let ((str (substring (symbol-name subtree) 0 4)))
                                (or (string= "eon!" str)
                                    (string= "eon:" str))))))
                      #'(lambda (subtree)
                          (funcall subtree))
                      tree))

(defvar eon-log-level t)

(setf eon-log-level :verbose)

(defvar eon-log-sequence-number 0)

(defun eon-reset-logging ()
  (setf eon-log-sequence-number 0))

(defun eon-log (format-string &rest args)
  (when eon-log-level
    (apply #'message
           (format "Eon: [%d] %s" eon-log-sequence-number format-string)
           args)
    (cl-incf eon-log-sequence-number)))

(defun eon-log-verbosely (format-string &rest args)
  (when (eq :verbose eon-log-level)
    (apply #'eon-log format-string args)))

(defvar eon-dataset ())
(setf eon-dataset ())

(defun eon-add-dataset-row (row)
  (push row eon-dataset))

(defun eon-expand-form (form)
  (let (error-flag)
    (let ((result-1 (condition-case err (eon-eval-scheme form)
                    (error
                     (prog1 err
                       (setf error-flag err)
                       (cl-incf eon|error-count)
                       (message "Note: Error %S in execution of form %S" err form))))))
      (let ((trace  
             (if (not error-flag)
                 (or (ignore-errors (eon-capture-trace form)) "")
               (or (ignore-errors (eon-capture-trace form)) "")))
            (result
             (if (not error-flag)
                 result-1
               ""))
            (status
             (or error-flag t)))
        (format "<s>[INST] [FORM] %S [/INST][RESULT] %S [TRACE] %s [STATUS] %s </s>"
                form result trace status)))))

(defvar eon-data-directory (expand-file-name "~/ai"))

(defun eon-data-file (file) (expand-file-name file eon-data-directory))

(cl-defmacro eon/percent-of-time (percent &body body)
  `(when (< (random 100) ,percent)
     ,@body))

(defvar eon-permute-p nil)

(cl-defmacro eon/with-permutations (&body body)
  `(let ((eon-permute-p t))
     ,@body))

(defun eon-permute (things)
  (if eon-permute-p 
      (let ((len (length things))
	    (things2 (coerce things 'vector)))
        (cl-dotimes (n len)
          (cl-rotatef (aref things2 n)
	              (aref things2 (random len))))
        (coerce things2 'list))
    things))

;;; Generators are zero-arg functions called "eon:PRIMITIVE"

(defun eon-generator-name (primitive)
  (intern (concat "eon:" (symbol-name primitive))))

(cl-defmacro eon/defgenerator (primitive &body body)
  (declare (indent defun))
  (let ((func (eon-generator-name primitive)))
    `(progn
       (defun ,func () ,@body)
       (put ',func 'eon-generator t)
       ',func)))

(defvar eon-seed-string "1138")
(defun eon-reset-seed ()
  (interactive)
  (random eon-seed-string))

(cl-defun eon!collect (generator &optional (count eon|width))
  (cl-loop for n from 1 to count
           collect (funcall generator)))

(defun eon!choose (set)
  (nth (random (length set))
       set))

(defun eon!true-or-false () (eon!choose `(,sie-true ,sie-false)))
(defun eon!zero-or-one () (eon!choose '(0 1)))
(defun eon!small-positive-integer () (1+ (random 10)))
(defun eon!small-integer () (- (random 20) 10))
(defun eon!small-negative-integer () (- 0 (1+ (random 10))))

(defun eon!string () 
  (mapconcat 'identity
             (cl-loop for n from 1 to (1+ (random 5))
                      collect (symbol-name (eon!choose eon-alphabet)))))

(defun eon!symbol ()
  (intern (eon!string)))

(cl-defun eon!begin (&rest contents)
  (cons 'begin (eon-permute contents)))

(cl-defun eon!list (&rest contents)
  (cons 'list
        (eon-permute contents)))

(cl-defun eon!vector (&rest contents)
  (coerce (eon-permute contents) 'vector))

(cl-defun eon!atom (&optional (types
                               '(integer integer nil vector
                                         string string
                                         symbol symbol symbol symbol)))
  (let ((type (eon!choose types)))
    (cl-case type
      (integer (- (random 2048) 1024))
      (symbol (intern (eon!string)))
      (vector (eon!vector (eon!random-list)))
      (string (eon!string))
      (t nil))))

(cl-defun eon!random-list (&optional (length eon|width))
  (if (zerop eon|depth)
      (eon!atom)
      (cl-loop for n from 1 to length collect
               (let ((eon|depth (1- eon|depth)))
                 (or (eon/percent-of-time 50 (eon!random-list (max 1 (1- length))))
                     (eon!atom))))))

(eon/defgenerator scheme-version `(scheme-version))

(defun scheme-version () "MarvinScheme v0.1")

(eon/defgenerator eval-in-emacs
  `(eval-in-emacs ',(or (eon/percent-of-time 50
                                             (eon!choose `((marvin-add-task ,(eon!choose eon|tasks))
                                                           (marvin-add-note ,(eon!string)))))
                        (eon!random-list))))

(defun eval-in-emacs (form)
  `(print '((emacs! ',form '(/emacs)))))

(eon/defgenerator play-bell-sound `(play-bell-sound))

(defun play-bell-sound ()
  `(emacs! (marvin-beep)))

(eon/defgenerator add-task `(add-task ,(eon!choose eon|tasks)))

(defun add-task (string)
  `(emacs! (marvin-add-task ,string)))

(eon/defgenerator add-note `(add-note ,(eon!choose eon|tasks)))

(defun add-note (string)
  `(emacs! (marvin-add-note ,string)))

(defun emacs! (&rest args)
  `(emacs! ,@args))
    
(eon/defgenerator string-append
  `(string-append ,@(cl-loop for i from 1 to eon|width collect (eon!string))))

;; (eon-capture-trace (eon:eval-in-emacs))
;; (eon-capture-trace '(string-append "abc" "def"))

(eon/defgenerator eq
  (let* ((atom-1 (eon!atom))
         (atom-2 (or (eon/percent-of-time 50 (eon!atom))
                     atom-1)))
    `(eq? ',atom-1 ',atom-2)))

(eon/defgenerator equal
  (let* ((list-1 (eon!random-list))
         (list-2 (or (eon/percent-of-time 50 (eon!random-list))
                     list-1)))
    `(equal? ',list-1 ,list-2)))

(eon/defgenerator string=
  (let* ((string-1 (eon!string))
         (string-2 (or (eon/percent-of-time 50 (eon!string))
                     string-1)))
    `(equal? ,string-1 ,string-2)))

(eon/defgenerator stringp
  (or (eon/percent-of-time 50 `(integer? ,(eon!atom)))
      `(integer? ,(eon!string))))

(eon/defgenerator sequencep
  `(sequence? ',(or (eon/percent-of-time 50 (eon!atom))
                 (eon/percent-of-time 50 (eon!random-list))
                 (coerce (eon!random-list) 'vector))))

(eon/defgenerator integerp
  (or (eon/percent-of-time 50 `(integer? ,(eon!atom)))
      `(integer? ,(eon!small-integer))))

(eon/defgenerator vectorp
  `(vector? ',(or (eon/percent-of-time 50 (eon!atom))
                 (eon/percent-of-time 50 (eon!random-list))
                 (coerce (eon!random-list) 'vector))))

(eon/defgenerator consp
  `(pair? ',(or (eon/percent-of-time 50 (eon!atom))
                 (eon/percent-of-time 50
                                      (coerce (eon!random-list) 'vector))
                 (eon!random-list))))

(eon/defgenerator evenp 
  `(even? ,(eon!small-integer)))

(eon/defgenerator oddp 
  `(odd? ,(eon!small-integer)))

(eon/defgenerator zerop 
  `(zero? ,(eon!small-integer)))

(eon/defgenerator print
  `(print ',(eon!random-list)))

(eon/defgenerator length
  `(length ',(eon!random-list)))

(eon/defgenerator reverse
  `(reverse ',(eon!random-list)))

(eon/defgenerator and 
  (cons 'and (cl-loop for n from 1 to eon|width collect (eon!true-or-false))))

(eon/defgenerator or 
  (cons 'or (cl-loop for n from 1 to eon|width collect (eon!true-or-false))))

(eon/defgenerator not (list 'not (eon!true-or-false)))
(eon/defgenerator % (list '% (eon!small-positive-integer) (eon!small-positive-integer)))
(eon/defgenerator 1+ (list '1+ (eon!small-integer)))
(eon/defgenerator 1- (list '1- (eon!small-integer)))

(eon/defgenerator arrayp (cons 'arrayp (eon!atom)))

(cl-defun eon!alist ()
  (cl-loop for n from 1 to eon|width
           collect (cons (eon!symbol) (eon!atom))))

(eon/defgenerator assoc ()
  (let* ((alist (eon!alist))
         (keys (mapcar #'car alist)))
    `(assoc ',(or (eon/percent-of-time 50 (eon!choose keys))
                 (eon!symbol))
            ',alist)))

(eon/defgenerator assq ()
  (let* ((alist (eon!alist))
         (keys (mapcar #'car alist)))
    `(assq ',(or (eon/percent-of-time 50 (eon!choose keys))
                 (eon!symbol))
            ',alist)))

(eon/defgenerator aset `(vector-set! ',(eon!atom) ,(eon!small-integer) ',(eon!atom)))

(eon/defgenerator atom 
  `(atom? ',(or (eon/percent-of-time 50 (list (eon!atom)))
               (eon!random-list eon|width))))

(eon/defgenerator bound? `(bound? ',(eon!symbol)))

(eon/defgenerator car `(car ',(eon!random-list)))
(eon/defgenerator cdr  `(cdr ',(eon!random-list)))
(eon/defgenerator cons `(cons ',(eon!atom) ',(eon!random-list)))
(eon/defgenerator + (cons '+ (eon!collect 'eon!small-integer)))
(eon/defgenerator - (cons '- (eon!collect 'eon!small-integer)))
(eon/defgenerator * (cons '* (eon!collect 'eon!small-integer)))
(eon/defgenerator / (cons '/ (eon!collect 'eon!small-positive-integer)))
(eon/defgenerator < (cons '< (eon!collect 'eon!small-integer)))
(eon/defgenerator <= (cons '<= (eon!collect 'eon!small-integer)))
(eon/defgenerator = (cons '= (eon!collect 'eon!small-integer)))
(eon/defgenerator > (cons '> (eon!collect 'eon!small-integer)))
(eon/defgenerator >= (cons '>= (eon!collect 'eon!small-integer)))

(eon/defgenerator append 
  (append
   (list 'append)
   (mapcar (lambda (x)
             `(quote ,x))
           (cl-loop for n from 1 to (1+ (random 3)) collect `',(eon!random-list)))))

(eon/defgenerator apply 
  `(apply
    ',(eon!choose '(append car cdr cons + - * / < <= = > >=))
    ',(cl-loop for n from 1 to eon|width collect (eon!atom))))

(eon/defgenerator aref 
  `(vector-ref ',(or (eon/percent-of-time 33
                                    (coerce (cl-loop for n from 1 to (1+ (random eon|width)) collect (eon!small-integer))
                                            'vector))
               (eon/percent-of-time 33
                                    (eon!string))
               (cl-loop for n from 1 to eon|width collect (eon!atom)))
         ,(eon!small-integer)))

(eon/defgenerator arrayp `(array? ',(eon!atom)))

(eon/defgenerator concat `(concat ,@(eon!collect 'eon!string eon|width)))

;;; Generating and exporting the dataset

(defun eon-find-generator (symbol)
  (let ((sym (intern (concat "eon:" (symbol-name symbol)))))
    (when (fboundp sym)
      sym)))

(defun eon-generate-dataset ()
  ;; (eon-reset-seed)
  (eon-reset-logging)
  (setf eon|error-count 0)
  (eon-log "Generating EON dataset...")
  (let ((num-records 0))
    (setf eon-dataset ())
    ;; first generate all primitives
    (eon-log "Generating code for %d primitives..." (length eon-primitives))
    (let ((generators (mapcar 'eon-find-generator eon-primitives)))
      (cl-dolist (generator generators)
        (when generator
          (eon-log "Running %d generations for primitive %S" eon|generations generator)
          (cl-loop for depth from 2 to eon|max-depth do
                   (cl-loop for width from 2 to eon|max-width do
                            (let ((eon|depth depth)
                                  (eon|width width))
                              (cl-dotimes (n eon|generations)
                                (eon-add-dataset-row (eon-expand-form (funcall generator)))
                                (cl-incf num-records)))))
          (eon-log "Now collected %d rows..." num-records))))
    (eon-log "Generated %d records for %d primitives." num-records (length eon-primitives))
    ;; next generate scenarios with the CFG
    (eon-log "Generating CFG scenarios...")
    (cl-dolist (start-symbol (eon-start-symbols))
      (eon-log "Generating scenarios for %S..." start-symbol)
      (cl-loop for depth from 2 to eon|max-depth do
               (cl-loop for width from 2 to eon|max-width do
                        (let ((eon|depth depth)
                              (eon|width width))
                          (cl-dotimes (n eon|num-scenarios)
                            (eon-add-dataset-row
                             (eon-expand-form
                              (eon-expand-generators
                               (eon-generate-phrase start-symbol))))
                            (cl-incf num-records))))))
    (prog1 eon-dataset
      (eon-log "Generated EON dataset with %d rows, of which %d create error conditions." num-records eon|error-count))))

(defvar eon-dataset-csv ())

(defun eon-export-csv (rows)
  (let ((csv (cons "form,trace,result,status" 
                   (mapcar #'(lambda (row)
                               (format "%S"
                                       (getf row :data)))
                           rows))))
    (mapcar #'(lambda (row)
                (with-temp-buffer
                  (insert row)
                  (goto-char (point-min))
                  (while (search-forward "\n" nil t)
                    (replace-match " "))
                  (concat (buffer-substring-no-properties (point-min) (point-max)))))
            csv)))

(cl-defun eon-export-dataset (&optional (dataset eon-dataset))
  (eon-log "Preparing dataset for JSON and CSV conversion...")
  (with-temp-buffer
    (let ((vec
           (apply 'vector
                  (mapcar (lambda (row)
                            (if (stringp row)
                                (list :data row)
                              (cl-destructuring-bind (form trace result status) row
                                (list :form (prin1-to-string form)
                                      :trace trace
                                      :result (prin1-to-string result)
                                      :status (prin1-to-string status)
                                      ))))
                          (eon/with-permutations (eon-permute dataset))))))
      (eon-log "Exporting dataset to CSV...")
      (setf eon-dataset-csv (eon-export-csv (coerce vec 'list)))
      (eon-log "Exporting dataset to JSON...")
      (json-encode vec))))

(cl-defun eon-save-dataset (&optional extra-entries)
  (interactive)
  (save-window-excursion
    (eon-generate-dataset)
    (eon-log "Generated dataset. Saving...")
    (with-temp-buffer (insert (eon-export-dataset (append (reverse eon-dataset) extra-entries)))
                      (write-file (eon-data-file "dataset.json"))
                      (eon-log "Saved dataset.json"))
    (with-temp-buffer (dolist (string eon-dataset-csv)
                        (insert string)
                        (insert "\n"))
                      (write-file (eon-data-file "dataset.csv"))
                      (eon-log "Saved dataset.csv"))))

;;; Capturing trace output

(defun eon-capture-trace (form)
  (with-current-buffer (get-buffer-create sie-trace-buffer)
    (delete-region (point-min) (point-max))
    (ignore-errors (eon-eval-scheme form))

    (goto-char (point-min))
    (while (search-forward "[sie-magic]" nil t)
      (replace-match "<m>"))

    (goto-char (point-min))
    (while (search-forward "sie-subr" nil t)
      (replace-match "<subr>"))

    (goto-char (point-min))
    (while (search-forward "sie-cont-funcall" nil t)
      (replace-match "<funcall>"))

    (goto-char (point-min))
    (while (search-forward "sie-cont-call" nil t)
      (replace-match "<call>"))

    (goto-char (point-min))
    (while (search-forward "sie-environment" nil t)
      (replace-match "<env>"))
    
    ;; (goto-char (point-min))
    ;; (while (search-forward "\\#t" nil t)
    ;;   (replace-match "#t"))

    (goto-char (point-min))
    (while (search-forward "[" nil t)
      (replace-match "("))

    (goto-char (point-min))
    (while (search-forward "]" nil t)
      (replace-match ")"))

    ;; Fix the Hey Emacs tags that were changed by the previous step
    ;; of removing square brackets.
    ;; (goto-char (point-min))
    ;; (while (search-forward "(EMACS)" nil t)
    ;;   (replace-match "[EMACS]"))
    ;; (goto-char (point-min))
    ;; (while (search-forward "(/EMACS)" nil t)
    ;;   (replace-match "[/EMACS]"))

    (buffer-substring-no-properties (point-min) (point-max))))

(defun eon-unimplemented-primitives ()
  (interactive)
  (let ((syms (cl-remove-if #'eon-find-generator eon-primitives)))
    (prog1 syms
      (message "There are %d unimplemented primitives: %S" (length syms) syms))))

;;; SIE (Scheme In Emacs) integration

(defvar eon-scheme-environment ())

(defun eon-primitive-subr-name (subr)
  (intern (concat "scm:" (symbol-name subr))))

(defun eon-make-primitive (subr)
  (eval `(progn
           (defun ,(eon-primitive-subr-name subr) (&rest args)
               (sie-action-result (apply ',subr args)))
           (sie-make-subr ',(eon-primitive-subr-name subr)))))

(defun eon-insinuate-scheme ()
  (interactive)
  (setf eon-scheme-environment
        (sie-make-environment sie-basic-environment))
  (dolist (primitive eon-primitives)
    (sie-environment-bind eon-scheme-environment
                          primitive
                          (eon-make-primitive primitive)))
  eon-scheme-environment)

(eon-insinuate-scheme)

(defun eon-eval-scheme (form)
  (sie-eval form eon-scheme-environment))

(eon-eval-scheme `(define true ,sie-true))
(eon-eval-scheme `(define t ,sie-true))
(eon-eval-scheme `(define false ,sie-false))

;;; Synthetic multi-turn English task conversation dataset

(defun eon-wrap-turn (turn)
  (cl-destructuring-bind (input output) turn
    (format "[INST] %s [/INST]%s</s> " input output)))

(defun eon-wrap-conversation (turns)
  (format "<s>%s\n"
          (mapconcat 'eon-wrap-turn turns)))

(defun eon-replace-variables (text)
  (with-temp-buffer
    (insert text)
    (goto-char (point-min))
    (while (re-search-forward "{\\(\\w\\|!\\)*}" nil t)
      (let ((str (match-string 0)))
        (replace-match (funcall (symbol-function (intern (substring str 1 (- (length str) 1))))))))
    (buffer-substring-no-properties (point-min) (point-max))))

(defun eon!confirm () (eon!choose '("Okay." "Sure." "No problem." "All right.")))

(defvar eon|task "Clean my room")

(defvar eon|tasks ())

(defun eon!task () eon|task)

(defun eon-set-task (task)
  (setf eon|task task))

(setf eon|tasks
      '("Clean my room"
         "Vaccuum the carpet"
         "Take out the trash"
         "Write a journal entry"
         "Design a new program"
         "Do a light workout"
         "Do a heavy workout"
         "Take a walk around the neighborhood"
         "Take 10 deep breaths"
         "Meditate for 10 minutes"
         "Tidy the garage"
         "Vaccuum the stairs"
         ))

(defvar eon|beta "happy")

(defvar eon|betas ())

(defun eon!beta () eon|beta)

(defvar eon|gamma "happy")

(defvar eon|gammas ())

(setf eon|gammas '("a" "b" "c" "d" "e" "f" "g" "h" "i" "j" "k" "l" "m" "n" "o" "p" "q" "r" "s" "t" "u" "v" "w" "x" "y" "z"))

(defun eon!gamma () eon|gamma)

(defvar eon-conversations ())

(setf eon-conversations
      '((("Please add a task."
          "{eon!confirm} What should the task be?")
         ("{eon!task}"
          "(emacs! '(marvin-add-task \"{eon!task}\"))"))
        (("Please take a note."
          "{eon!confirm} What should the note be?")
         ("{eon!gamma}"
          "(emacs! '(marvin-add-task \"{eon!gamma}\"))"))
        (("Take a note."
          "{eon!confirm} What should I take a note of?")
         ("My current mood is {eon!beta}."
          "(emacs! '(marvin-add-note \"Current mood: {eon!beta}\")"))
        (("Make Emacs beep."
          "(emacs! '(marvin-beep))"))
        (("Tell Emacs to beep."
          "(emacs! '(marvin-beep))"))
        (("Please make Emacs beep."
          "(emacs! '(marvin-beep))"))
        (("Please beep."
          "(emacs! '(marvin-beep))"))
        ))

(cl-defun eon-conversation-rows (&optional (conversations eon-conversations))
  (let (results)
    (cl-dolist (c conversations)
      (cl-dotimes (n eon|generations)
        (eon-set-task (eon!choose eon|tasks))
        (setf eon|beta (eon!choose '("happy" "sad" "ebullient" "relaxed" "joyful" "sorrowful")))
        (setf eon|gamma (eon!choose eon|gammas))
        (push (eon-replace-variables (eon-wrap-conversation c))
              results)))
    (reverse results)))

(defun eon-export ()
  (interactive)
  (let ((print-level nil)
        (print-length nil)
        (marvin-suppress-beep t))
    (eon-save-dataset)))
;    (eon-conversation-rows)))

;; (eon-export)

(provide 'eon)
;;; eon.el ends here
