;;; sd.el --- working with stable diffusion          -*- lexical-binding: t; -*-

;; Copyright (C) 2023  David O'Toole

;; Author: David O'Toole <dto@thexder>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

(require 'marvin)

(defcustom marvin-diffusion-directory "~/ai/"
  "Directory where diffusion scripts and output images are stored."
  :tag "Marvin diffusion directory"
  :group 'marvin
  :type 'directory)

(defcustom marvin-diffusion-device "cuda"
  "Pytorch device identifier for Stable Diffusion compute."
  :tag "Marvin diffusion device"
  :group 'marvin
  :type 'string)

(defun marvin-diffusion-script-file (id)
  (expand-file-name (format "marvin-%d.py" id) marvin-diffusion-directory))

(defun marvin-prompt-output-file (id)
  (expand-file-name (format "marvin-%d.prompt.txt" id) marvin-diffusion-directory))


(cl-defun marvin-text-to-image (&key (height 512) (width 512)
                                      prompt output-prefix
                                      (negative-prompt "")
                                      id (steps 50) (guidance 0.75)
                                      (seed (random 293747932)))
  (with-temp-buffer
    (let ((template "from os import environ
import random
import torch
from diffusers import StableDiffusionPipeline

prompt = %S
negative_prompt = %S
seed = %d
generator = torch.Generator(\"cuda\").manual_seed(seed)
output_file = \"%s\" + str(%d) + \"-\" + str(seed) + \".png\"
steps = %d
guidance = %f

model_id = \"/home/dto/ai/stable-diffusion-2-1\"

pipe = StableDiffusionPipeline.from_pretrained(
    model_id,
    torch_dtype=torch.float32,
    use_auth_token=True)

pipe = pipe.to(%S)
image = pipe(prompt, negative_prompt=negative_prompt, height=%d, width=%d, guidance_scale=guidance, num_inference_steps=steps, generator=generator)[\"sample\"][0]  
image.save(output_file)
"))
      (insert (format template prompt negative-prompt seed output-prefix id steps guidance "cuda" height width))
      (write-file (marvin-diffusion-script-file id))
      (with-temp-buffer
        (insert prompt)
        (write-file (marvin-prompt-output-file id))))))

(defun marvin-random-seed () (random 249872498))

(cl-defun marvin-generate-images (&key (n 5) (steps 50)
                                        prompt negative-prompt
                                        (guidance 0.75)
                                        marvin-diffusion-device
                                        (height 512) (width 512)
                                        seed)
  (let (scripts)
    (dotimes (i n)
      (marvin-text-to-image :prompt prompt
                              :output-prefix "marvin-"
                              :steps steps
                              :id i
                              :seed (or seed (marvin-random-seed))
                              :guidance guidance
                              :height height
                              :width width)
      (push (marvin-diffusion-script-file i) scripts))
    (reverse scripts)))

(provide 'sd)
;;; sd.el ends here
