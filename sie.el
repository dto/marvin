;;; sie.el --- Scheme In Emacs (v2)

;; Copyright (C) 1999 by Kalle Niemitalo

;; Author: Kalle Niemitalo <tosi@stekt.oulu.fi>
;; Maintainer: David O'Toole <deeteeoh1138@gmail.com>
;; Keywords: local, lisp

;; This file is not part of GNU Emacs, but the same conditions apply.

;; GNU Emacs is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; Symbols, numbers, strings and pairs are directly represented as the
;; corresponding Emacs Lisp types.
;;
;; The empty list is represented as the Emacs Lisp `nil'.
;; The Scheme symbol `nil' is represented by the value of the variable
;; `sie-nil'.
;;
;; Vectors are represented as Emacs Lisp vectors whose first element
;; is not `eq' to the value of the variable `sie-magic'.
;;
;; Other types are represented as vectors whose first element is `eq'
;; to the value of the variable `sie-magic'.  Cons cells could be used
;; instead, but vectors are rarer so we don't have to check them as
;; often.
;;
;; Booleans:
;;   #f:  `[,sie-magic sie-false]
;;   #t:  `[,sie-magic sie-true]
;;   These vectors are kept in variables `sie-false' and `sie-true' so
;;   SIE Booleans can be compared with `eq'.
;;
;; Characters:
;;   `[,sie-magic sie-char ,integer]
;;
;; Lambda procedures:
;;   `[,sie-magic sie-lambda ,args ,body ,environment]
;;
;; Subroutines:
;;   `[,sie-magic sie-subr ,elisp-function]
;;   The elisp-function is called with the evaluated arguments.
;;   It must return an evaluator action.
;;
;; Syntaxes:
;;   `[,sie-magic sie-syntax ,elisp-function]
;;   The elisp-function is called with two arguments: list of
;;   unevaluated arguments in the call, and the environment.
;;   It must return an evaluator action.
;;
;; Environments:
;;   `[,sie-magic sie-environment ,binding-alist ,inherited-env]
;;   These are not directly accessible by Scheme code.
;;   But they are mutable via `define'.
;;
;; Continuations:
;;   `[,sie-magic sie-continuation ,stack-pointer]
;;   See sie-eval.el for information about the stack.
;;
;; Unspecified:
;;   `[,sie-magic sie-unspecified]
;;   This vector is kept in the variable `sie-unspecified'.

;;; Code:

(require 'cl-lib)

(defun sie-check-type (predicate arg)
  "Signal `wrong-type-argument' if (predicate arg) returns nil.
This error happens in the Emacs Lisp level,
so don't check Scheme values with this function."
  (or (funcall predicate arg)
      (signal 'wrong-type-argument (list predicate arg))))

(defconst sie-magic
  (if (boundp 'sie-magic) sie-magic
    (vector 'sie-magic))
  "Magic object for used for encoding Scheme values.
If the first element of a vector is `eq' to `sie-magic', that vector
represents a Scheme object of a type which does not exist in Emacs
Lisp.")

(defun sie-magical-p (object)
  "Return t if OBJECT is a vector but represents something else."
  (and (vectorp object)
       (eq (aref object 0) sie-magic)))

(defalias 'sie-number-p 'numberp)
(defalias 'sie-string-p 'stringp)

(defun sie-vector-p (object)
  "Return t if OBJECT represents a Scheme vector."
  (and (vectorp object)
       (not (eq (aref object 0) sie-magic))))

;; Symbol

(defconst sie-nil
  (make-symbol "sie-nil")
  "Emacs representation of Scheme nil symbol.")

(defun sie-symbol-p (object)
  "Return t if OBJECT represents a Scheme symbol."
  (and (symbolp object)
       object))

;; Boolean

(defconst sie-false
  (if (boundp 'sie-false) sie-false
    (vector sie-magic 'sie-false))
  "Emacs representation of Scheme #f.")

(defconst sie-true
  (if (boundp 'sie-true) sie-true
    (vector sie-magic 'sie-true))
  "Emacs representation of Scheme #t.")

(defun sie-bool-p (object)
  "Return t if OBJECT represents Scheme #f or #t."
  (or (eq object sie-false)
      (eq object sie-true)))

;; Unspecified

(defconst sie-unspecified
  (if (boundp 'sie-unspecified) sie-unspecified
    (vector sie-magic 'sie-unspecified))
  "Emacs representation of unspecified Scheme value.")

(defun sie-unspecified-p (object)
  "Return t if OBJECT represents an unspecified Scheme value."
  (eq object sie-unspecified))

;; List

(defalias 'sie-pair-p 'consp)
(defalias 'sie-null-p 'null)

;; (sie-car nil) returns nil, but that's no problem,
;; since you should check sie-pair-p before calling.
(defalias 'sie-car 'car)
(defalias 'sie-cdr 'cdr)

(defun sie-list-p (object)
  "Return non-nil if OBJECT is a Scheme list.
Doesn't detect circular lists, so they cause an infinite loop."
  (while (sie-pair-p object)
    (setq object (sie-cdr object)))
  (sie-null-p object))

;; A nice recursive version:
;; (defun sie-arglist-p (object)
;;   (or (sie-null-p object)
;; 	 (sie-symbol-p object)
;; 	 (and (sie-pair-p object)
;; 	      (sie-symbol-p (sie-car object))
;; 	      (sie-arglist-p (sie-cdr object)))))
;; but Emacs Lisp isn't good at recursion, so...
(defun sie-arglist-p (object)
  "Return non-nil if OBJECT is a Scheme argument list.
This means it is a possibly improper list consisting of symbols.

All of these are argument lists:
	()
	(first second third)
	(first . rest)
	all-args"
  (catch 'return
    (while (sie-pair-p object)
      (unless (sie-symbol-p (sie-car object))
	(throw 'return nil))
      (setq object (sie-cdr object)))
    (or (sie-null-p object)
	(sie-symbol-p object))))

;; Character

(defun sie-make-char (charid)
  "Return a new SIE character."
  (sie-check-type #'integerp charid)
  (vector sie-magic 'sie-char charid))

(defun sie-char-p (object)
  "Return t if OBJECT represents a Scheme character."
  (and (sie-magical-p object)
       (eq (aref object 1) 'sie-char)))

;; Lambda

(defun sie-make-lambda (args body-forms environment)
  "Return a new SIE lambda procedure."
  (sie-check-type #'sie-arglist-p args)
  (sie-check-type #'sie-list-p body-forms)
  (sie-check-type #'sie-environment-p environment)
  (vector sie-magic 'sie-lambda args body-forms environment))

(defun sie-lambda-p (object)
  "Return t if OBJECT is a SIE lambda procedure."
  (and (sie-magical-p object)
       (eq (aref object 1) 'sie-lambda)))

(defun sie-lambda-args (lambda-procedure)
  "Return the arguments of LAMBDA-PROCEDURE.
LAMBDA-PROCEDURE must be sie-lambda-p."
  (aref lambda-procedure 2))

(defun sie-lambda-body (lambda-procedure)
  "Return the body forms of LAMBDA-PROCEDURE.
LAMBDA-PROCEDURE must be sie-lambda-p."
  (aref lambda-procedure 3))

(defun sie-lambda-environment (lambda-procedure)
  "Return the environment of LAMBDA-PROCEDURE.
LAMBDA-PROCEDURE must be sie-lambda-p."
  (aref lambda-procedure 4))

;; Subroutine

(defun sie-make-subr (elisp-function)
  "Return a new SIE subroutine."
  (sie-check-type #'functionp elisp-function)
  (vector sie-magic 'sie-subr elisp-function))

(defun sie-subr-p (object)
  "Return t if OBJECT is a SIE subroutine."
  (and (sie-magical-p object)
       (eq (aref object 1) 'sie-subr)))

(defun sie-subr-function (subr)
  "Return the Emacs Lisp function corresponding to SUBR.
SUBR must be sie-subr-p."
  (aref subr 2))

;; Syntax

(defun sie-make-syntax (elisp-function)
  "Return a new SIE syntax."
  (sie-check-type #'functionp elisp-function)
  (vector sie-magic 'sie-syntax elisp-function))

(defun sie-syntax-p (object)
  "Return t if OBJECT is a SIE syntax."
  (and (sie-magical-p object)
       (eq (aref object 1) 'sie-syntax)))

(defun sie-syntax-function (syntax)
  "Return the Emacs Lisp function corresponding to SYNTAX.
SYNTAX must be sie-syntax-p."
  (aref syntax 2))

;; Environment

(defun sie-make-environment (inherited)
  "Return a new SIE environment."
  (when inherited
    (sie-check-type #'sie-environment-p inherited))
  (vector sie-magic 'sie-environment '() inherited))

(defun sie-environment-p (object)
  "Return t if OBJECT is a SIE environment."
  (and (sie-magical-p object)
       (eq (aref object 1) 'sie-environment)))

(defun sie-environment-bindings (environment)
  "Return the binding alist of ENVIRONMENT.
The car of each binding is SYMBOL, and the cdr is its value."
  (aref environment 2))

(defun sie-environment-binding (environment symbol)
  "Return the binding of SYMBOL in ENVIRONMENT, or nil.
The car of the binding is SYMBOL, and the cdr is its value."
  (assq symbol (sie-environment-bindings environment)))

(defun sie-environment-inherited (environment)
  "Return the environment inherited by ENVIRONMENT, or nil.
ENVIRONMENT must be sie-environment-p."
  (aref environment 3))

(defun sie-environment-bind (environment symbol value)
  "Bind SYMBOL to VALUE in ENVIRONMENT."
  (let ((binding (sie-environment-binding environment symbol)))
    (if binding
	(setcdr binding value)
      (aset environment 2 (cons (cons symbol value) (aref environment 2))))))

;; Continuation

(defun sie-make-continuation (stack)
  "Return a new SIE continuation."
  (sie-check-type #'listp stack)
  (vector sie-magic 'sie-continuation stack))

(defun sie-continuation-p (object)
  "Return t if OBJECT is a SIE continuation."
  (and (sie-magical-p object)
       (eq (aref object 1) 'sie-continuation)))

(defun sie-continuation-stack (continuation)
  "Return the stack of CONTINUATION.
CONTINUATION must be sie-continuation-p."
  (aref continuation 2))

;;; Printing values.

;; Functions for converting SIE values to strings and printing
;; evaluation traces.

(defun sie-prin1-to-string (object &optional noescape)
  (let ((print-level (and print-level (1- print-level))))
    (cond
     ((eq object sie-nil)
      "nil")
     ((sie-null-p object)
      "()")
     ((sie-bool-p object)
      (if (eq object sie-false) "#f" "#t"))
     ((sie-unspecified-p object)
      "#<unspecified>")
     ((sie-environment-p object) "<env>")
      ;; (if (and print-level (< print-level 0))
      ;;     "#<environment...>"
      ;;   (concat "#<environment: bindings="
      ;;   	(sie-prin1-to-string (sie-environment-bindings object) noescape)
      ;;   	(let ((inherited (sie-environment-inherited object)))
      ;;   	  (if inherited
      ;;   	      (concat " inherits="
      ;;   		      (sie-prin1-to-string inherited noescape))))
      ;;  	">")))
     ((or (sie-symbol-p object)
	  (sie-number-p object))
      (prin1-to-string object noescape))
     ((sie-pair-p object)
      (if (and print-level (< print-level 0))
	  "..."
	(let ((str (concat "(" (sie-prin1-to-string (sie-car object)))))
	  (setq object (sie-cdr object))
	  (while (sie-pair-p object)
	    (setq str (concat str " "
			      (sie-prin1-to-string (sie-car object)))
		  object (sie-cdr object)))
	  (if object
	      (concat str " . " (sie-prin1-to-string object) ")")
	    (concat str ")")))))
     ((sie-lambda-p object)
      (if (and print-level (< print-level 0))
	  "#<lambda...>"
	(concat "#<lambda: args="
		(sie-prin1-to-string (sie-lambda-args object) noescape)
		" body-forms="
		(sie-prin1-to-string (sie-lambda-body object) noescape)
		" <env>"
		;;(sie-prin1-to-string (sie-lambda-environment object) noescape)
		">")))
     ((sie-subr-p object)
      (let ((str (prin1-to-string (sie-subr-function object) noescape)))
        (save-match-data (if (string-match "sie-subr-" str)
                             (substring str 9)
                           (substring str 4)))))
     ((sie-syntax-p object)
      (concat "#<syntax "
	      (prin1-to-string (sie-syntax-function object) noescape)
	      ">"))
     ((sie-string-p object)
      ;;object)
      (format "%S" object))
     ((sie-continuation-p object)
      (if (and print-level (< print-level 0))
	  "#<continuation...>"
	(concat "#<continuation: stack="
		(sie-prin1-to-string (sie-continuation-stack object))
		">")))
     ((vectorp object)
      (format "#%S" (coerce object 'list)))
     (t
      (error "SIE: Don't know how to print: %S" object)))))

(defun sie-prin1 (object &optional printcharfun)
  (princ (sie-prin1-to-string object) printcharfun))

(defun sie-print (object &optional printcharfun)
  (terpri printcharfun)
  (princ (sie-prin1-to-string object) printcharfun)
  (terpri printcharfun))

(defun sie-format (format &rest data)
  "Like Emacs' builtin `format' but %S means a SIE value."
  (setq format (copy-sequence format)
	data (copy-sequence data))
  (let ((p data)
	(index nil))
    (while (setq index (string-match "%-?[0-9]*\\([%a-zA-Z]\\)"
				     format index))
      (unless (string= (match-string 1 format) "%")
	(when (string= (match-string 1 format) "S")
	  (setcar p (sie-prin1-to-string (car p)))
	  (aset format (match-beginning 1) ?s))
	(setq p (cdr p)))
      (setq index (1+ index))))
  (apply #'format format data))

(defun sie-error (format &rest data)
  (error "SIE: %s"
	 (apply #'sie-format format data)))

(defvar sie-trace-buffer "*SIE trace*"
  "*Buffer where `sie-trace' prints its messages, or nil to disable.")

(defun sie-trace (format &rest data)
  (when sie-trace-buffer
    (with-current-buffer (get-buffer-create sie-trace-buffer)
      (save-excursion
	(goto-char (point-max))
	(when (boundp 'stack)
	  (insert (make-string (length stack) ?.)))
	(insert-before-markers
	 (let ((print-level nil))
	   (apply #'sie-format format data))
	 "\n")))))

;;; Evaluation engine.

;; This is a Scheme-like evaluator written in Emacs Lisp.  The
;; evaluator can not recurse in Emacs Lisp level, because Scheme must
;; be tail-recursive and Emacs Lisp isn't.  Instead, the evaluator
;; stack is an Emacs Lisp list.  This also gives us cheap
;; continuations since lists can have a common tail.  (SIE v2 actually
;; had continuations before it had `lambda'!)
;;
;; When the evaluator has finished some computation and has its
;; result, it pops an entry from the stack.  Each entry is a list
;; whose car is an Emacs Lisp function ("stack function").  By
;; convention, names of stack functions begin with "sie-cont-".  The
;; evaluator calls the stack function, passing the previous result to
;; it as the first parameter.  The cdr of each stack entry is a
;; (possibly empty) list of other parameters.
;;
;; The stack function then decides what to do next, and returns an
;; "action" saying what to do next.  Actions are created by calling
;; one of the functions whose names begin with "sie-action-".
;; `sie-eval' is the only function which parses actions.
;;
;; When a stack function needs to evaluate some expression before it
;; can continue, it uses `sie-action-push&next'.  When the evaluator
;; has finished evaluating the expression, it comes back to the entry
;; pushed on the stack.  Sometimes, it's useful to make the entry
;; point back to the function which caused it to be pushed.
;; `sie-cont-funcall' processes an arbitrarily long argument list this
;; way.
;;
;; Evaluating an expression also requires an environment in which
;; symbols are looked up.  `sie-action-next' and
;; `sie-action-push&next' need the environment as a parameter.  The
;; evaluator does not automatically associate stack entries with
;; environments; usually, the environment is one of the parameters in
;; the cdr of a stack entry.

(defun sie-print-action (action)
  (if (null action)
      (sie-trace "Null action")
    (cl-case (first action)
      (push&next (sie-trace "Push: %S" (caddr action)))
      (next (sie-trace "Next: %S" (cons (cadr action) nil)))
      (jump&result (sie-trace "Value: %S" (cddr action)))
      (result (sie-trace "Value: %S" (cdr action)))
      (otherwise (sie-trace "Other...")))))

(defun sie-eval (initial-form initial-environment)
  "Evaluate Scheme FORM in ENVIRONMENT and return the result.
This is never called recursively, since it creates a new stack." 
  (sie-trace "Evaluate: %S" initial-form)
  (let ((stack '())
	(action (sie-action-next initial-form initial-environment)))
    (catch 'return
      (while t
	;; (sie-trace "Action: %S" action)
        (sie-print-action action)
	(cond ((eq (car action) 'push&next)
	       (setq stack (cons (cadr action) stack))
	       (setq action (sie-eval-1 (caddr action) (cadddr action))))
	      ((eq (car action) 'next)
	       (setq action (sie-eval-1 (cadr action) (caddr action))))
	      ((eq (car action) 'jump&result)
	       (setq stack (cadr action))
	       (setq action (sie-action-result (cddr action))))
	      ((eq (car action) 'result)
	       (let ((result (cdr action)))
		 (if (null stack)
		     (throw 'return result)
		   (let ((popped (car stack)))
		     (setq stack (cdr stack))
		     ;; (sie-trace "Pop: %S" popped)
		     (setq action (apply (car popped) result (cdr popped)))))))
	      (t
		(error "SIE: Invalid evaluator action: %S" action)))))))

(defun sie-action-result (result)
  "Return an action meaning the form evaluates to RESULT."
  (cons 'result result))

(defun sie-action-next (next environment)
  "Return an action causing the evaluator to evaluate NEXT instead.
This allows tail recursion."
  (list 'next next environment))

(defun sie-action-jump&result (stack result)
  "Return an action causing the evaluator to jump to STACK.
This is used by continuations."
  (cons 'jump&result (cons stack result)))

(defun sie-action-push&next (push next env)
  "Return an action causing the evaluator to push PUSH on the stack and
evaluate NEXT in ENV to get a value which is then applied to the stack frame.

The CAR of PUSH is a stack function, and the CDR is a list of
parameters for the function.  The evaluated value of NEXT is given to
the function before the extra parameters."
  (list 'push&next push next env))

(defun sie-eval-1 (form environment)
  "Evaluate part of FORM in ENVIRONMENT and return an evaluator action
saying what to do next."
  (cond
   ((or (sie-number-p form)
	(sie-string-p form)
	(sie-bool-p form)
	(sie-char-p form))
    (sie-action-result form))
   ((sie-symbol-p form)
    (sie-action-result (sie-symbol-value form environment)))
   ((sie-pair-p form)
    (sie-action-push&next (list #'sie-cont-call (cdr form) environment)
			  (car form)
			  environment))
   (t
    (sie-error "Invalid form: %S" form))))

(defun sie-symbol-value (symbol environment)
  "Return the value of Scheme SYMBOL in ENVIRONMENT.
Does *not* return an evaluator action, since the operation
doesn't involve calling any Scheme functions."
  (catch 'return
    (while environment
      (let ((binding (sie-environment-binding environment symbol)))
	(when binding
	  ;; (sie-trace "Found binding: %S" binding)
	  (throw 'return (cdr binding)))
	(setq environment (sie-environment-inherited environment))))
    (sie-error "No binding for symbol: %S" symbol)))

(defun sie-cont-call (fn raw-args environment)
  "Stack function for a Scheme macro or function call.
Checks whether FN is a macro or a function, and branches accordingly.
Returns an evaluator action."
  (cond ((sie-syntax-p fn)
	 (sie-trace "Calling syntax: %S" (cons fn raw-args))
	 (funcall (sie-syntax-function fn) raw-args environment))
	((sie-null-p raw-args)
	 (sie-apply fn '()))
	((sie-action-push&next (list #'sie-cont-funcall
				     fn
				     '()
				     (cdr raw-args)
				     environment)
			       (car raw-args)
			       environment))))

(defun sie-cont-funcall (new-arg fn evalled-args raw-args environment)
  "Stack function for a Scheme function call.
Arranges RAW-ARGS to be evaluated in ENVIRONMENT one by one and
collects them in EVALLED-ARGS which is in reverse order.
Unreverses the list and calls FN when RAW-ARGS is empty.
Returns an evaluator action."
  (setq evalled-args (cons new-arg evalled-args))
  (if (sie-null-p raw-args)
      (sie-apply fn (reverse evalled-args))
    (sie-action-push&next (list #'sie-cont-funcall
				fn
				evalled-args
				(cdr raw-args)
				environment)
			  (car raw-args)
			  environment)))

(defun sie-apply (fn args)
  "Call the Scheme function FN with ARGS.
FN may be a lambda, a subroutine or a continuation.
Unlike Scheme or Lisp `apply', this function takes only one list of arguments.
Returns an evaluator action."
  (sie-trace "Calling: %S" (cons fn args))
  (cond ((sie-subr-p fn)
	 (apply (sie-subr-function fn) args))
	((sie-continuation-p fn)
	 (unless (= (length args) 1)
	   (sie-error "Continuation needs one argument"))
	 (sie-action-jump&result (sie-continuation-stack fn) (car args)))
	((sie-lambda-p fn)
	 (let ((body (sie-lambda-body fn))
	       (env (sie-build-lambda-env (sie-lambda-args fn)
					  args
					  (sie-lambda-environment fn))))
	   (sie-cont-returnlast sie-unspecified body env)))
	(t
	 (sie-error "Invalid function: %S" fn))))

(defun sie-cont-returnlast (ignored-prev-result forms environment)
  "Stack function for `lambda' or `begin'.
Evaluates FORMS one by one, ignores results and jumps to the last one,
thus enabling tail recursion.
Returns an evaluator action."
  (if (sie-null-p (sie-cdr forms))
      (sie-action-next (sie-car forms) environment)
    (sie-action-push&next (list #'sie-cont-begin (cdr forms))
			  (car forms)
			  environment)))

(defun sie-build-lambda-env (formal-args actual-args inherited-env)
  "Return a new environment which binds FORMAL-ARGS to ACTUAL-ARGS
and inherits INHERITED-ENV."
  (let ((env (sie-make-environment inherited-env)))
    (while (consp formal-args)
      (sie-environment-bind env (car formal-args) (car actual-args))
      (setq formal-args (cdr formal-args)
	    actual-args (cdr actual-args)))
    (if formal-args			; rest argument
	(sie-environment-bind env formal-args actual-args))
    env))

;;; Primitive subroutines.

(defconst sie-empty-environment
  (sie-make-environment nil)
  "SIE environment containing no bindings at all.")

(defvar sie-minimal-environment
  (sie-make-environment sie-empty-environment)
  "SIE environment containing very few bindings.")

;; lambda

(defun sie-syn-lambda (args environment)
  "The Scheme `lambda' syntax: (lambda ARGS BODY...)
Returns an evaluator action."
  (unless (>= (length args) 2)
    (sie-error "`lambda' needs at least 2 arguments: %S" args))
  (sie-action-result
   (sie-make-lambda (sie-car args) (sie-cdr args) environment)))

(sie-environment-bind sie-minimal-environment
		      'lambda (sie-make-syntax #'sie-syn-lambda))

;; define

(defun sie-syn-define (args environment)
  "The Scheme `define' syntax:
\(define VAR VALUE)
\(define (FUNCTION [ARG]... [. REST]) FORM...)
Returns an evaluator action."
  (cond ((sie-symbol-p (sie-car args))
	 (unless (= (length args) 2)
	   (sie-error "`define VARIABLE' needs 2 arguments: %S" args))
	 (sie-environment-bind environment
			       (sie-car args)
			       (sie-car (sie-cdr args))))
	((sie-pair-p (sie-car args))
	 (let ((symbol (sie-car (sie-car args)))
	       (arglist (sie-cdr (sie-car args)))
	       (body (sie-cdr args)))
	   (unless (sie-arglist-p arglist)
	     (sie-error "Invalid argument list in `define (FUNCTION)': %S"
			arglist))
	   (sie-environment-bind environment symbol
				 (sie-make-lambda arglist body environment))))
	(t
	 (sie-error "Invalid `define'")))
  (sie-action-result sie-unspecified))

(sie-environment-bind sie-minimal-environment
		      'define (sie-make-syntax #'sie-syn-define))

;; quote

(defun sie-syn-quote (args ignored-environment)
  "The Scheme `quote' syntax: (quote OBJECT)
Returns an evaluator action."
  (unless (= (length args) 1)
    (sie-error "`quote' needs 1 argument: %S" args))
  (sie-action-result (car args)))

(sie-environment-bind sie-minimal-environment
		      'quote (sie-make-syntax #'sie-syn-quote))

;; if

(defun sie-syn-if (args environment)
  "The Scheme `if' syntax: (if TEST THEN [ELSE])
Returns an evaluator action."
  (unless (memq (length args) '(2 3))
    (sie-error "`if' needs 2 or 3 arguments: %S" args))
  (sie-action-push&next (list #'sie-cont-if (cdr args) environment)
			(car args)))

(defun sie-cont-if (evalled-test then&else environment)
  "Stack function for the Scheme `if' syntax.
Returns an evaluator action."
  (if (eq evalled-test sie-false)
      (if (null (cdr then&else))
	  (sie-action-result sie-unspecified)
	(sie-action-next (cdr then&else) environment))
    (sie-action-next (car then&else) environment)))

(sie-environment-bind sie-minimal-environment
		      'if (sie-make-syntax #'sie-syn-if))

;; begin
;; This cannot be defined in Scheme, since it must be tail-recursive.

(defun sie-syn-begin (args environment)
  "The Scheme `begin' syntax: (begin FORM...)
Returns an evaluator action."
  (unless (sie-pair-p args)
    (sie-error "`begin' needs at least one argument"))
  (if (sie-null-p (sie-cdr args))	; only one argument
      (sie-action-next (sie-car args) environment)
    (sie-action-push&next (list #'sie-cont-returnlast
				(sie-cdr args)
				environment)
			  (sie-car args)
			  environment)))

(sie-environment-bind sie-minimal-environment
		      'begin (sie-make-syntax #'sie-syn-begin))


(defvar sie-basic-environment
  (sie-make-environment sie-minimal-environment))

;; call-with-current-continuation

(defun sie-subr-call/cc (procedure)
  "The Scheme `call-with-current-continuation' procedure:
\(call-with-current-continuation PROCEDURE)
Returns an evaluator action."
  ;; This uses the dynamic scope established by sie-eval; ugly!
  (sie-apply procedure (list (sie-make-continuation stack))))

(sie-environment-bind sie-basic-environment 'call/cc
		      (sie-make-subr #'sie-subr-call/cc))
(sie-environment-bind sie-basic-environment 'call-with-current-continuation
		      (sie-make-subr #'sie-subr-call/cc))

;; list

(defun sie-subr-list (&rest args)
  "The Scheme `list' procedure: (list [ITEM]...)
Returns an evaluator action."
  (sie-action-result args))

(sie-environment-bind sie-basic-environment 'list
		      (sie-make-subr #'sie-subr-list))

;; noop

(defun sie-subr-noop (arg)
  "The Scheme `noop' procedure: (noop ARG)
Returns an evaluator action."
  (sie-action-result arg))

(sie-environment-bind sie-basic-environment 'noop
		      (sie-make-subr #'sie-subr-noop))


;;; Interaction with Emacs user

;; Commands for interactive use of SIE: various ways to evaluate, and
;; symbol completion.

(require 'cmuscheme)

(defvar sie-evaluation-environment 'sie-basic-environment
  "*Environment used by `sie-eval-last-sexp' and `sie-eval-defun'.
This may be a real environment or the name of a variable containing one.")

(defun sie-evaluation-environment ()
  "Return the value of variable `sie-evaluation-environment',
converted to an environment if it was the name of another variable."
  (if (symbolp sie-evaluation-environment)
      (symbol-value sie-evaluation-environment)
    sie-eval-environment))

(defun sie-eval-last-sexp (sie-eval-last-sexp-arg-internal)
  "Evaluate SIE sexp before point; print value in minibuffer.
With argument, print output into current buffer."
  (interactive "P")
  (let ((standard-output (if sie-eval-last-sexp-arg-internal (current-buffer) t)))
    (sie-prin1 (sie-eval (let ((stab (syntax-table))
			   (opoint (point))
			   expr)
			   (unwind-protect
			       (save-excursion
				 (set-syntax-table scheme-mode-syntax-table)
				 (forward-sexp -1)
				 (save-restriction
				   (narrow-to-region (point-min) opoint)
				   (read (current-buffer))))
			     (set-syntax-table stab)))
			 (sie-evaluation-environment)))))

(defun sie-eval-print-last-sexp ()
  "Evaluate SIE sexp before point; print value into current buffer."
  (interactive)
  (let ((standard-output (current-buffer)))
    (terpri)
    (sie-eval-last-sexp t)
    (terpri)))

(defun sie-eval-defun (sie-eval-defun-arg-internal)
  "SIE-evaluate defun that point is in or before.
Print value in minibuffer.
With argument, insert value in current buffer after the defun."
  (interactive "P")
  (save-excursion
    (let ((standard-output (if sie-eval-defun-arg-internal (current-buffer) t))
	  (form (progn
		  (end-of-defun)
		  (beginning-of-defun)
		  (read (current-buffer)))))
      (sie-print (sie-eval form (sie-evaluation-environment))))))

(defvar sie-completion-environment 'sie-basic-environment
  "*Environment from which `sie-symbol-completions' takes symbols.
This may be an environment or the name of a variable containing one.")

(defun sie-completion-environment ()
  "Return the value of variable `sie-completion-environment',
converted to an environment if it was the name of another variable."
  (if (symbolp sie-completion-environment)
      (symbol-value sie-completion-environment)
    sie-completion-environment))

(defun sie-symbol-names ()
  (let ((env (sie-completion-environment))
	(names '()))
    (while env
      (setq names (nconc (mapcar (lambda (binding)
				   (list (symbol-name (car binding))))
				 (sie-environment-bindings env))
			 names)
	    env (sie-environment-inherited env)))
    names))

(defun sie-complete-symbol ()
  (interactive)
  (let* ((end (point))
	 (beginning (save-excursion
		      (skip-syntax-backward "w_")
		      (point)))
	 (pattern (buffer-substring beginning end))
	 (symbol-names (sie-symbol-names))
	 (completion (try-completion pattern symbol-names)))
    (cond ((eq completion t))
	  ((null completion)
	   (message "Can't find completion for \"%s\"" pattern)
	   (ding))
	  ((not (string= pattern completion))
	   (delete-region beginning end)
	   (insert completion))
	  (t
	   (message "Making completion list...")
	   (let ((list (all-completions pattern symbol-names)))
	     (with-output-to-temp-buffer "*Completions*"
	       (display-completion-list list)))
	   (message "Making completion list...%s" "done")))))

(define-derived-mode sie-interaction-mode
  lisp-interaction-mode "SIE Interaction"
  (substitute-key-definition 'eval-last-sexp 'sie-eval-last-sexp
			     (current-local-map) global-map)
  (substitute-key-definition 'eval-print-last-sexp 'sie-eval-print-last-sexp
			     (current-local-map))
  (substitute-key-definition 'eval-defun 'sie-eval-defun
			     (current-local-map))
  (substitute-key-definition 'lisp-complete-symbol 'sie-complete-symbol
			     (current-local-map))
  (set-syntax-table scheme-mode-syntax-table))
  
(provide 'sie)

;;; sie.el ends here
