(defvar gpu-per-second)
(defvar cpu-per-second)
(defvar estimate-unit-basis)
(defvar seconds-per-unit)
(defvar num-cores)

;; (setf gpu-per-second 0.00046716) ;; A10G
(setf gpu-per-second 0.00091388) ;; A100-40
(setf cpu-per-second 0.00002778)
(setf estimate-unit-basis 3132)
(setf seconds-per-unit (* 60 60))
(setf num-cores 4)

(defun estimated-factor ()
  (/ (float seconds-per-unit)
     (float estimate-unit-basis)))

(defun seconds-for-records (num-records)
  (* num-records (estimated-factor)))

(defun minutes-for-records (num-records)
  (/ (seconds-for-records num-records)
     60.0))

(defun money-for-records (num-records)
  (let ((total-seconds (seconds-for-records num-records)))
    (+ (* gpu-per-second total-seconds)
       (* cpu-per-second total-seconds num-cores))))

;; (/ (minutes-for-records 50000) 60.0)
;; (money-for-records 3132)
       

;; (defvar estimate 
