;;; marvin.el --- AI assistant for Emacs  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  David O'Toole

;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Keywords: tools, ai

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; The Software is provided "as is", without warranty of any kind,
;; express or implied, including but not limited to the warranties of
;; merchantability, fitness for a particular purpose and
;; noninfringement. In no event shall the authors or copyright holders
;; be liable for any claim, damages or other liability, whether in an
;; action of contract, tort or otherwise, arising from, out of or in
;; connection with the Software or the use or other dealings in the
;; Software.

;;; Commentary:

;; WARNING: Large Language Models are a new technology and you should
;; be careful with this program. The base model used for Marvin,
;; called Mistral Instruct 7B, is unmoderated and may produce
;; undesired outputs. A default moderation module is included; please
;; see the file asimov.mrv.

;; For more information please see the included README file.

(eval-when-compile (require 'cl))
(require 'cl-lib)
(require 'hy-mode)
(require 'eieio)

;;; Code:

;;; A class to hide model/prompt template details, allow multiple
;;; models, support moderation, etc.

(defvar *marvin* nil)

(defun marvin-self () *marvin*)

(defclass marvin ()
  ((system-prompt :initform "" :initarg :system-prompt :accessor marvin-system-prompt)
   (prompt-template :initform "%s" :initarg :prompt-template :accessor marvin-prompt-template)
   (learning-template :initform "Human: %s\nAI: %s\n" :initarg :learning-template :accessor marvin-learning-template)
   (lisp-template :initform "[INST] %s [/INST]")
   (model-file :initform "" :initarg :model-file :accessor marvin-model-file)
   (default-temperature :initform 0.1138 :initarg :default-temperature :accessor marvin-default-temperature)))

(defclass marvin-alpha-7b (marvin)
  ;; This is the exact pattern:
   ;; "<s>[INST] U1 [/INST]R1</s> [INST] U2 [/INST]R2</s> "
  ((system-prompt :initform "<s>[INST] %s [/INST]")
   (prompt-template :initform "[INST] %s [/INST]")
   (lisp-template :initform "[INST] [FORM] %s [/INST]")
   (learning-template :initform "Request: %s\nResult: %s\n\n")
;;   (model-file :initform "marvin-alpha-7b-q4_0.gguf")
   (model-file :initform "marvin-alpha-7b-exported")
   (default-temperature :initform 0.6)))

(defclass marvin-mistral-instruct-7b (marvin)
  ((system-prompt :initform "<s>[INST] %s [/INST]\n")
   (prompt-template :initform "[INST] %s [/INST]\n")
   (learning-template :initform "Request: %s\nResult: %s\n\n")
   (model-file :initform "mistral-7b-instruct-v0.1.Q4_0.gguf")
   (default-temperature :initform 0.001138)))

(defclass marvin-mistral-code-7b (marvin)
  ((system-prompt :initform "Below is an instruction that describes a task. Write a response that appropriately completes the request.\n### Instruction:\n%s\n### Response:\n")
   (prompt-template :initform "Below is an instruction that describes a task. Write a response that appropriately completes the request.\n### Instruction:\n%s\n### Response:\n")
   (learning-template :initform "Human: %s\nAI: %s\n\n")
   (model-file :initform "mistral-7b-code-16k-qlora.Q4_0.gguf")
   (default-temperature :initform 0.1138)))

(defclass marvin-mistral-lite-7b (marvin)
  ((system-prompt :initform "<|prompter|>%s</s><|assistant|>")
   (prompt-template :initform "<|prompter|>%s</s><|assistant|>")
   (learning-template :initform "Human: %s\nAI: %s\n\n")
   (model-file :initform "mistrallite.Q4_0.gguf")
   (default-temperature :initform 0.1138)))

(defclass marvin-rift-coder-7b (marvin)
  ((system-prompt :initform "<s>[INST]\n<<SYS>>\n%s\n<</SYS>>\n\n%s\n[/INST]\n")
   (prompt-template :initform "<s>[INST]\n%s\n[/INST]\n")
   (model-file :initform "rift-coder-v0-7b-q4_0.gguf")))

(defclass marvin-falcon-7b (marvin)
  ((system-prompt :initform "### Instruction:\n%s\n### Response:\n ")
   (prompt-template :initform "### Instruction:\n%s\n### Response:\n")
   (model-file :initform "gpt4all-falcon-q4_0.gguf")))

(cl-defmethod marvin-expand-system-prompt ((self marvin) first-prompt)
  (with-slots (system-prompt) self
    (format system-prompt
            (marvin-metaprompt-string)
            first-prompt)))

(cl-defmethod marvin-expand-system-prompt ((self marvin-mistral-instruct-7b) first-prompt)
  (with-slots (system-prompt) self
    (format system-prompt
            (concat (marvin-metaprompt-string) first-prompt))))

(cl-defmethod marvin-expand-system-prompt ((self marvin-alpha-7b) first-prompt)
  (with-slots (system-prompt) self
    "<s>[INST] [FORM] (+ 2 3) [/INST]"))
    ;; (format system-prompt
    ;;         (concat (marvin-metaprompt-string) first-prompt))))

(cl-defmethod marvin-expand-lisp-prompt ((self marvin-alpha-7b) form)
  (with-slots (lisp-template) self
    (format lisp-template form)))

(cl-defmethod marvin-expand-system-prompt ((self marvin-mistral-code-7b) first-prompt)
  (with-slots (system-prompt) self
    (format system-prompt
            (concat (marvin-metaprompt-string) "\n" first-prompt))))

(cl-defmethod marvin-expand-system-prompt ((self marvin-mistral-lite-7b) first-prompt)
  (with-slots (system-prompt) self
    (format system-prompt
            (concat (marvin-metaprompt-string) "\n" first-prompt))))

(cl-defmethod marvin-expand-system-prompt ((self marvin-falcon-7b) first-prompt)
  (with-slots (system-prompt) self
    (format system-prompt
            (concat (marvin-metaprompt-string) "\n" first-prompt))))

(cl-defmethod marvin-expand-prompt ((self marvin) prompt)
  (format (marvin-prompt-template self)
          prompt))

;; This function calls embedded Hy code; see below.

(cl-defmethod marvin-begin-session ((self marvin) &optional (first-prompt ""))
  (with-slots (model-file default-temperature) self
    (setf marvin-model (marvin-data-file model-file))
    (if marvin-use-automatic-temperature-control
        (setf marvin-temperature default-temperature)
      (message "Marvin: beginning session with automatic temperature control disabled...")))
  (marvin-clear-forms)
  (marvin-stop-timer)
  (marvin-start-timer)
  (marvin-run-hy)
  (marvin^do-preamble)
  (marvin^define-llm)
  ;; (marvin^create-chat-session)
  (marvin^run (marvin-expand-system-prompt self first-prompt))
  ;;(marvin^print-output)
  (marvin^save-output)
  (marvin-save-forms (marvin-data-file "marvin.hy"))
  (marvin-send-all-forms)
  (switch-to-buffer marvin-log-buffer-name)
  ;; (delete-other-windows)
  (goto-char (point-max)))

(defvar marvin-personality-signature "c2c6fe24-7fcf-4bc7-93ea-a4186f81a65f")

(defcustom marvin-temperature 0.6
  "Value between 0 and 1. It is recommended to let Marvin manage the
temperature automatically by setting
`marvin-use-automatic-temperature-control' to a non-nil value."
  :type 'number :group 'marvin)

(defcustom marvin-number-of-threads 6
  "Number of threads to use for LLM."
  :type 'integer :group 'marvin)

(defcustom marvin-device "gpu"
  "Which device to use for LLM."
  :type 'string :group 'marvin)

(defgroup marvin () "Marvin AI assistant")

(defcustom marvin-user-emoji "👨‍🦰"
  "The emoji string that identifies the user in the log.
This can be any string and doesn't require emoji."
  :type 'string :group 'marvin)

(defcustom marvin-user-name "David"
  "The name of the user."
  :type 'string :group 'marvin)

(defcustom marvin-assistant-emoji "👨"
  "The emoji string that identifies the assistant in the log."
  :type 'string :group 'marvin)

(defcustom marvin-assistant-name "Marvin"
  "The name of the AI assistant."
  :type 'string :group 'marvin)

;;; Finding files

(defcustom marvin-data-directory
  (file-name-as-directory (expand-file-name "~/ai"))
  "Directory where the assistant will read models from and store data to."
  :group 'marvin)

(defcustom marvin-source-directory (expand-file-name "~/marvin")
  "Directory where the Marvin kernel, modules, and sounds are loaded from."
  :group 'marvin)

(defun marvin-source-file (file)
  "Find FILE within the marvin source directory."
  (expand-file-name file marvin-source-directory))

(defun marvin-data-file (file)
  "Find FILE within the marvin directory."
  (expand-file-name file marvin-data-directory))

(defun marvin-kernel-source ()
  (with-temp-buffer
    (insert-file-contents (marvin-source-file "marvin.mrv"))
    (buffer-substring-no-properties (point-min) (point-max))))

;;; To send commands to the LLM we need to access the Hy buffer.

(defvar marvin-buffer-name "*Hy*")

(defun marvin-buffer ()
  (or (get-buffer marvin-buffer-name)
      (error "Marvin: No Hy process")))

(defun marvin-quit-hy ()
  (interactive)
  (ignore-errors (kill-buffer (marvin-buffer))))

(defun marvin-run-hy ()
  (interactive)
  (ignore-errors (marvin-quit-hy))
  (run-hy))

;; Once every second, Emacs will check on the LLM to see if it has
;; delivered any output.

(defvar marvin-timer-interval 1)

(defvar marvin-timer nil)

(defun marvin-start-timer ()
  (when (timerp marvin-timer)
    (cancel-timer marvin-timer))
  (setf marvin-timer
        (run-with-timer 0 marvin-timer-interval 'marvin-update)))

(defun marvin-stop-timer ()
  (interactive)
  (when (timerp marvin-timer)
    (cancel-timer marvin-timer)))

(defun marvin-start ()
  (interactive)
  (marvin-start-timer))

(defvar marvin-outputs nil)

(defun marvin-output-file ()
  (marvin-data-file "output.txt"))

(defun marvin-output-file-exists ()
  (file-exists-p (marvin-output-file)))

(defun marvin-delete-output-file ()
  (delete-file (marvin-output-file)))

(defun marvin-capture-output ()
  (ignore-errors (push (with-temp-buffer
                         (insert-file-contents (marvin-output-file))
                         (buffer-substring-no-properties (point-min) (point-max)))
                       marvin-outputs)
                 (marvin-log-output (car marvin-outputs))
                 (dolist (func marvin-process-output-functions)
                   (unless marvin-disable-eval-in-emacs
                     (funcall func (car marvin-outputs)))))
  (marvin-delete-output-file))

;;; We can load several modules in sequence.

(defvar marvin-module-queue ())
(defvar marvin-currently-loading-module nil)
(defvar marvin-modules-being-loaded nil)

(defun marvin-clear-module-queue ()
  (setf marvin-module-queue ()))

(defun marvin-queue-modules (modules)
  (setf marvin-modules-being-loaded modules)
  (setf marvin-module-queue modules))

(defun marvin-queued-modules-p ()
  (not (null marvin-module-queue)))

(defun marvin-pop-queued-module ()
  (pop marvin-module-queue))

(defun marvin-module-source (module)
  (with-temp-buffer
    (insert-file-contents (marvin-source-file module))
    (buffer-substring-no-properties (point-min) (point-max))))

(defun marvin-send-module (module)
  (if (vectorp module)
      (progn
        (setf marvin-currently-loading-module "<standard-input>")
        (marvin-do (aref module 0)))
    (progn
      (setf marvin-currently-loading-module module)
      (marvin-do (marvin-module-source module)))))

;;; Here's the update function that runs every second.

(defvar marvin-number-of-updates 0)

(defun marvin-update ()
  (when (marvin-output-file-exists)
    (marvin-capture-output)
    (when (marvin-queued-modules-p)
      (marvin-send-module (marvin-pop-queued-module))
      (when (not (marvin-queued-modules-p))
        (setf marvin-currently-loading-module nil))))
  (when marvin-outputs
    (dolist (output (reverse marvin-outputs))
      (marvin-say output))
    (setf marvin-outputs nil))
  (unless (marvin-speaking-p)
    (when marvin-speech-queue
      (marvin-say-now (marvin-pop-speech-queue))))
  (marvin-update-header-line)
  (cl-incf marvin-number-of-updates))

;;; Animated busy/idle indicator

(defvar marvin-hy-prompt-regexp "^=> ")

(defun marvin-hy-prompt-p ()
  (with-current-buffer (marvin-buffer)
    (save-excursion
      (goto-char (point-max))
      (looking-back marvin-hy-prompt-regexp (- (point) 20)))))

(defcustom marvin-progress-characters "🕐🕑🕒🕓🕔🕕🕖🕗🕘🕙🕚🕛"
  "A string with characters used to indicate progress."
  :type 'string
  :group 'marvin)
  
(defun marvin-update-header-line ()
  (when (get-buffer-create marvin-log-buffer-name)
    (with-current-buffer (get-buffer marvin-log-buffer-name)
      (setf header-line-format
            (concat (if (not (marvin-hy-prompt-p))
                        (format "State: %s  |  %s "
                                (propertize "Busy" 'face '(:background "tomato" :foreground "yellow"))
                                (make-string 1 (aref marvin-progress-characters
                                                     (mod marvin-number-of-updates
                                                          (length marvin-progress-characters)
                                                          ))))
                      (format "State: %s  |  %s  |  Model: %s " 
                              (propertize "Idle" 'face '(:background "seagreen" :foreground "yellow"))
                              marvin-elapsed-time-string
                              (prin1-to-string (class-name (class-of (marvin-self))))
                              ))
                    (if marvin-currently-loading-module
                        (let ((pos (cl-position marvin-currently-loading-module
                                                marvin-modules-being-loaded
                                                :test 'equal)))
                          (when pos
                            (format "Loading module %s (%d/%d)..."
                                    marvin-currently-loading-module
                                    (1+ (or pos 0))
                                    (length marvin-modules-being-loaded))))
                      ""))))))


;;; The "Hey Emacs" protocol.

;; Marvin can execute selected functions within Emacs. This works by
;; having Emacs scan the output for special delimiters that the LLM
;; has been taught to generate.

(defvar marvin-process-output-functions nil)

(add-hook 'marvin-process-output-functions 'marvin-run-hey-emacs)

(defun marvin-run-hey-emacs (string)
  (with-temp-buffer
    (insert string)
    (goto-char (point-min))
    (marvin-eval-safely (marvin-hey-emacs-grab))))

(defvar marvin-safe-functions ())

(setf marvin-safe-functions 
      '(magit-status describe-function describe-variable
                     customize-group customize-variable customize marvin-beep
                     marvin-add-task marvin-add-note info ibuffer dired message
                     warn org-agenda marvin-save-memory))

(defun marvin-eval-safely (form)
  (let ((func (car form)))
    (when form
      (if (member func marvin-safe-functions)
          (progn
            ;; Don't evaluate the argument forms
            (condition-case err (progn (eval `(apply ',(car form) ',(cdr form)))
                                       (message "Marvin: Executed form %S" form))
              (error (warn (format "Marvin: Error %S in execution of form %S" err form)))))
        (progn
          (marvin-sad-beep)
          (warn "Marvin: Cannot evaluate potentially unsafe form %S" form))))))

;; Sending S-expressions to the Hy interpreter

(defun marvin-send-string (string)
  (save-window-excursion
    (switch-to-buffer (marvin-buffer))
    (goto-char (point-max))
    (insert string)
    (comint-send-input nil t)))

(defun marvin-fix-carets (string)
  "Emacs Lisp can't parse dots in symbol names, so we use carets
instead and fix them before sending to Hy."
  (with-temp-buffer
    (insert string)
    (goto-char (point-min))
    (while (search-forward "^" nil t)
      (replace-match "."))
    (buffer-substring-no-properties (point-min) (point-max))))

(defvar marvin-forms ())

(defun marvin-clear-forms ()
  (setf marvin-forms nil))

(defun marvin-queue-form (form)
  (push form marvin-forms))

(cl-defmacro marvin^ (&body body)
  `(marvin-queue-form '(do ,@body)))

(defun marvin-get-forms ()
  (reverse marvin-forms))

(defun marvin-send-form (form)
  (marvin-send-string
   (marvin-fix-carets (prin1-to-string form))))

(defun marvin-send-all-forms ()
  (mapc #'marvin-send-form (marvin-get-forms)))

(defun marvin-output-ready-p ()
  (not (null marvin-outputs)))

(defun marvin-pop-outputs ()
  (pop marvin-outputs))

;;; Now we begin with embedded Hy, which becomes Python.

(defun marvin^do-preamble ()
  (marvin-queue-form
   `(do
      (setv marvin-response False)
      (setv marvin-postamble "")
      (setv marvin-assistant-name ,marvin-assistant-name)
      (setv marvin-user-name ,marvin-user-name)
      (setv marvin-past-key-values False)
      (import contextlib)
      (import numpy)
      (import torch)
      (import torch [Tensor])
      (import tokenizers [Tokenizer])
      (import transformers [AutoModelForCausalLM])
      (import transformers [AutoTokenizer])
      (import transformers [StoppingCriteria])
      (import transformers [StoppingCriteriaList])
      (import transformers [GenerationConfig])
      (import transformers [BitsAndBytesConfig])
      (setv bnb-config (BitsAndBytesConfig
                        :load_in_4bit True
                        :bnb_4bit_use_double_quant True
                        :bnb_4bit_quant_type "nf4"
                        :bnb_4bit_compute_dtype torch^bfloat16)))))

(defcustom marvin-model nil
  "Model file for the LLM." 
  :group 'marvin)

(defvar marvin-max-tokens 4096)

;;; Instantiating the AI language model

(defun marvin^define-llm ()
  (marvin-queue-form
   `(do
      (setv marvin-llm (AutoModelForCausalLM^from_pretrained
                        ,marvin-model
                        :quantization-config bnb-config
                        ))
      (setv marvin-tokenizer (AutoTokenizer^from_pretrained ,marvin-model))
      (setv marvin-tokenizer^pad-token marvin-tokenizer^eos-token)
      (setv marvin-tokenizer^pad-token-id marvin-tokenizer^eos-token-id)
      (setv marvin-generation-config (GenerationConfig :max-new-tokens ,marvin-max-tokens
                                                       :do-sample True
                                                       :use-cache True
                                                       :output-attentions True
                                                       :return-attention-mask True
                                                       :return-dict-in-generate True
                                                       :pad-token-id marvin-tokenizer^pad-token-id
                                                       :eos-token-id marvin-tokenizer^eos-token-id
                                                       :bos-token-id marvin-tokenizer^bos-token-id
                                                       :temperature ,marvin-temperature))
      (setv marvin-stop-words ["[INST]"])
      (setv marvin-stop-words-ids (lfor word marvin-stop-words
                                        (^encode marvin-tokenizer word)))
      (defclass StoppingCriteriaSub [StoppingCriteria]
        (defn __init__ [self stops]
              (StoppingCriteria^__init__ self))
        (defn __call__ [self input_ids scores [stops []]]
              (setv self^stops stops)
              (lfor i (range 0 (len stops))
                    (setv self^stops (get self^stops i)))))
      (setv marvin-stopping-criteria
            (StoppingCriteriaList [(StoppingCriteriaSub marvin-stop-words-ids)])))))

(cl-defun marvin^run (string &key (max-tokens marvin-max-tokens))
  (marvin-queue-form
   `(do (setv marvin-prompt ,string)
        (setv marvin-input-ids (^encode marvin-tokenizer marvin-prompt :return-tensors "pt"))
      (when (not (= False marvin-response))
        (print (^keys marvin-response)))
      (setv marvin-response
            ;; (^to-tuple
             (if (= False marvin-past-key-values)
                 (^generate marvin-llm
                            :inputs (^to marvin-input-ids "cuda")
                            :generation-config marvin-generation-config
                            :output-attentions True
                            :return-attention-mask True
                            :return-dict-in-generate True
                            :stopping-criteria marvin-stopping-criteria)
               (do
                   ;; (setv marvin-inputs {})
                   ;; (setv (get marvin-inputs "input_ids") (^to marvin-input-ids "cuda"))
                   ;; (setv new-attention-len
                   ;;       (get (^shape (^__getitem__ marvin-response "past_key_values")) -1))
                   ;; (setv (get marvin-inputs "attention_mask")
                   ;;       (torch^nn^functional^pad
                   ;;        (get marvin-inputs "attention_mask")
                   ;;        (Tuple 0 (- new-attention-len
                   ;;                    (get (^shape (^__getitem__ marvin-response "attention_mask")) 1)))
                   ;;        :mode "constant"
                   ;;        :value 1))
                   (^generate marvin-llm
                              :inputs (^to marvin-input-ids "cuda")
                              :past-key-values marvin-past-key-values
                              :return-dict-in-generate True
                              :output-attentions True
                              :return-attention-mask True
                              :generation-config marvin-generation-config
                              :stopping-criteria marvin-stopping-criteria))))
      (setv marvin-past-key-values (^__getitem__ marvin-response "past_key_values"))
      (setv marvin-decoded-response (^decode marvin-tokenizer (get (^__getitem__ marvin-response "sequences") 0)))
      False
      )))

(defun marvin^create-chat-session ()
  (marvin^
   (setv chat-session (contextlib^ExitStack))
   (chat_session^enter_context (marvin-llm^chat_session :prompt-template "{0}"))))

(defun marvin^close-chat-session ()
  (chat-session^close))
                         
(defun marvin^load-document (var file)
  (marvin-queue-form `(do (setv ,var (marvin-document))
                          (^load ,var ,file))))

(defun marvin^collect-splits ()
  (marvin^
   (setv marvin-all-splits False)
   (lfor doc marvin-documents
         (if (= False marvin-all-splits)
             (setv marvin-all-splits doc^splits)
           (setv marvin-all-splits (+ marvin-all-splits doc^splits))))))

(cl-defun marvin^run-query (query &optional (num-results 4))
  (marvin-queue-form
   `(do (setv mychunks [])
        (setv marvin-query-result (^similarity_search marvin-vectorstore :query ,query :k ,num-results))
      (lfor doc marvin-query-result
            (setv mychunks (+ mychunks [(+ "Marvin, here is a context chunk: {" (str doc) "}.")])))
      (setv marvin-postamble mychunks))))

(cl-defun marvin-inject-results ()
  (interactive)
  (marvin-clear-forms)
  (marvin^ (print (str marvin-postamble)))
  (marvin-queue-form 
   `(lfor chunk marvin-postamble
          (do
              (setv marvin-response (marvin-llm^generate :prompt chunk :max-tokens 32))
              (print chunk)
            (setv output-file (open ,(marvin-data-file "output.txt") "w"))
            (^write output-file marvin-response)
            (^close output-file))))
  (marvin-send-all-forms))
 
(defun marvin^save-output ()
  (marvin-queue-form `(do (setv output-file (open ,(marvin-data-file "output.txt") "w"))
                          (^write output-file marvin-decoded-response)
                        (^close output-file))))

(defun marvin^print-output ()
  (marvin-queue-form
   `(do (print marvin-response))))
        ;; (setv marvin-tokens [])
        ;; (lfor i (range marvin-llm^model^context^tokens_size)
        ;;       (do (print (get marvin-llm^model^context^tokens i))
        ;;           (setv marvin-tokens (+ marvin-tokens [(get marvin-llm^model^context^tokens i)]))))
      ;; (^decode marvin-tokenizer marvin-tokens))))
               
(defun marvin-save-forms (file)
  (with-temp-buffer
    (insert (marvin-fix-carets (pp (marvin-get-forms) nil)))
    (let ((most (buffer-substring-no-properties (1+ (point-min)) (- (point-max) 2))))
      (with-temp-buffer
        (insert most)
        (write-file file)))))
  
;;; Using the MBROLA speech synthesizer.

(defvar marvin-speech-program "espeak")

(defvar marvin-speech-parameters 
  '(:voice "-v"
    :volume "-a"
    :pitch "-p"
    :markup "-m"
    :speed "-s"
    :punctuation "--punct"
    :file "-w"))

(defvar marvin-speech-additional-parameters '("--stdin" "-l" "0" "-m"))

(defun marvin-make-speech-parameter (entry)
  (cl-destructuring-bind (name . value) entry
    (let ((option (getf marvin-speech-parameters name)))
      (when option
	(list option 
	      (if (stringp (first value))
		  (first value)
		  (format "%S" (first value))))))))

(defun marvin-pairwise (x)
  (when (consp x)
    (cons (list (first x) (second x))
	  (marvin-pairwise (rest (rest x))))))

(defun marvin-make-speech-parameters (babel-params)
  (apply #'append 
	 (mapcar #'marvin-make-speech-parameter
		 (marvin-pairwise babel-params))))

(defun marvin-speech-command-string (params)
  (mapconcat 'identity 
	     (append (list marvin-speech-program)
		     marvin-speech-additional-parameters
		     (marvin-make-speech-parameters params))
	     " "))

(defun marvin-speech-file (params) 
  (getf params :file))

(defun marvin-speech-render (text params file)
  (let ((command
	  (marvin-speech-command-string 
	   (if (null file) 
	       params
	       (setf params (plist-put params :file file))))))
    (with-temp-buffer (insert text)
      (shell-command-on-region (point-min) (point-max) command))))

(defvar marvin-speech-process nil)
(defvar marvin-speech-queue nil)

(defun marvin-speech-play (file)
  (setf marvin-speech-process (start-process "*marvin-speech*" "*marvin-speech*" "play" file)))

(defun marvin-speaking-p ()
  (if (null marvin-speech-process)
      nil
    (not (eq (process-status marvin-speech-process) 'exit))))

(defvar marvin-voices nil)
(defvar marvin-voice nil)
(defvar marvin-voice-key nil)

(defun marvin-queue-speech (string)
  (setf marvin-speech-queue (append marvin-speech-queue (list string))))

(defun marvin-pop-speech-queue ()
  (pop marvin-speech-queue))

(defmacro define-voice (name &rest args)
  `(push ',(cons name args) marvin-voices))

(defun marvin-voice-parameters (name &rest args)
  (rest (assoc name marvin-voices)))

(defmacro with-voice (voice &rest body)
  `(let ((marvin-voice (marvin-voice-parameters ,voice))
	 (marvin-voice-key ,voice))
     ,@body))

(define-voice :computer :pitch 7 :speed 155 :voice mb-en1)

(defcustom marvin-use-voice-synthesis t
  "When non-nil, use voice synthesis."
  :type 'boolean :group 'marvin)

(defun marvin-speech-file () (marvin-data-file "marvin.wav"))

(defun marvin-say-now (text)
  (when marvin-use-voice-synthesis 
    (setf text (with-temp-buffer (insert text)
                                 ;; remove some things that are annoying
                                 ;; to have spoken
                                 (goto-char (point-min))
                                 (while (search-forward "<" nil t)
                                   (replace-match " "))
                                 (goto-char (point-min))
                                 (while (search-forward ">" nil t)
                                   (replace-match " "))
                                 (goto-char (point-min))
                                 (while (search-forward "**" nil t)
                                   (replace-match " "))
                                 (goto-char (point-min))
                                 ;; ;; if the model glitches, don't speak
                                 ;; ;; the glitch or anything after it
                                 (when (search-forward "### Human:" nil t)
                                   (replace-match "")
                                   (delete-region (point) (point-max)))
                                 (buffer-substring-no-properties (point-min) (point-max))))
    (let ((file (marvin-speech-file)))
      (with-voice :computer (marvin-speech-render text marvin-voice file))
      (marvin-speech-play file))))

(defun marvin-say (text)
  (marvin-queue-speech text))

(defun marvin-preprocess-prompt (prompt)
  "Remove any junk from the prompt." 
  (with-temp-buffer
    (insert prompt)
    (goto-char (point-min))
    (while (search-forward "[BLANK_AUDIO]" nil t)
      (replace-match ""))
    (goto-char (point-min))
    ;; (while (search-forward "[INST]" nil t)
    ;;   (replace-match ""))
    ;; (goto-char (point-min))
    ;; (while (search-forward "[/INST]" nil t)
    ;;   (replace-match ""))
    (string-trim (buffer-substring-no-properties (point-min) (point-max)))))

(cl-defun marvin-do (prompt)
  (interactive "sText to send to assistant: ")
  (marvin-clear-forms)
  (marvin-log-input prompt)
  (let ((trimmed-prompt (string-trim prompt)))
    (if (= ?\( (aref trimmed-prompt 0))
        (marvin^run (marvin-expand-lisp-prompt (marvin-self) trimmed-prompt))
      (marvin^run (marvin-expand-prompt (marvin-self) trimmed-prompt))))
  ;;(marvin^print-output)
  (marvin^save-output)
  (marvin-send-all-forms))

(defun marvin-metaprompt-string ()
  (with-temp-buffer 
    (insert (format "Today's date is %s. "
                    (format-time-string "%B %d, %Y" (current-time))))
    (insert (format "Your name is %s. You are my AI assistant. My name is %s."
            marvin-assistant-name marvin-user-name))
    (buffer-substring-no-properties (point-min) (point-max))))

(defun marvin-lisp-metaprompt-string ()
  "(+ 2 2)")

(defun marvin-hey-emacs-grab ()
  (when (re-search-forward marvin-hey-emacs-regexp nil t)
    (re-search-backward marvin-hey-emacs-regexp nil t)
    (let ((beg (point)))
      (let ((end (point-at-eol)))
        (let ((string (buffer-substring-no-properties beg end)))
          (message "Marvin: Grabbing Hey Emacs string %S" string)
          (cadr (car (read-from-string string))))))))

(defvar marvin-hey-emacs-regexp "(emacs! ")

  ;; (marvin^do-preamble)
  ;; (marvin^define-document-class)
  ;; (marvin^define-document-database)
  ;; (marvin^define-llm)
  ;; (marvin^create-chat-session)
  ;; (marvin^load-document 'marvin-elisp "/home/dto/ai/elisp.pdf")
  ;; (marvin^add-document-to-database 'marvin-elisp)
  ;; (marvin^collect-splits)
  ;; (marvin^create-vectorstore)

(defun marvin-stop ()
  (interactive)
  (setf marvin-outputs nil)
  (setf marvin-speech-queue nil)
  (ignore-errors
    (when marvin-speech-process
        (kill-process marvin-speech-process)
        (setf marvin-speech-process nil))))

(defun marvin-insinuate-keys ()
  (interactive)
  (global-set-key [(control escape)] 'marvin-stop)
  (global-set-key [(f12)] 'whisper-run))

(defvar marvin-suppress-beep nil)

(defun marvin-beep ()
  (interactive)
  (unless marvin-suppress-beep
    (marvin-speech-play "/home/dto/marvin/happy-sound.wav"))
  `(emacs! '(marvin-beep) '(/emacs)))

(defcustom marvin-use-click-sound t
  "When non-nil, make a little sound when assistant's response is
ready."
  :type 'boolean :group 'marvin)

(defun marvin-click ()
  (interactive)
  (when marvin-use-click-sound
    (marvin-speech-play "/home/dto/marvin/click-sound.wav")))

(defun marvin-sad-beep ()
  (interactive)
  (marvin-speech-play "/home/dto/marvin/sad-sound.wav"))

(cl-defun marvin-query (query &key (num-results 4))
  (interactive "sDatabase query: ")
  (marvin-clear-forms)
  (marvin^run-query query num-results)
  (marvin-send-all-forms)
  (marvin-inject-results))

(defun marvin-add-task (string)
  (with-temp-buffer
    (insert (concat "* TODO " string "\n"))
    (append-to-file (point-min) (point-max) (marvin-data-file "tasks.org"))))

(cl-defun marvin-add-note (string &key (content ""))
  (with-temp-buffer
    (insert (concat "* Note from Marvin: "
                    (format-time-string "[%Y-%m-%d %H:%M:%S]")
                    "\n"
                    string "  "content "\n"))
    (append-to-file (point-min) (point-max) (marvin-data-file "notes.org"))))

(defun marvin-eval-region ()
  (interactive)
  (marvin-do (buffer-substring-no-properties (region-beginning) (region-end))))

(defun marvin-eval-buffer ()
  (interactive)
  (marvin-do (buffer-substring-no-properties (point-min) (point-max))))

(defvar marvin-log-buffer-name "*marvin*")

(defun marvin-log-input (string)
  (let ((setup-p (null (get-buffer marvin-log-buffer-name))))
    (with-current-buffer (get-buffer-create marvin-log-buffer-name)
      (local-set-key [(return)] 'marvin-ret)
      (local-set-key (kbd "RET") 'marvin-ret)
      (goto-char (point-max))
      (insert "\n")
      (insert (concat marvin-user-emoji " "))
      (insert (propertize
               (marvin-preprocess-prompt string)
               'face '(:background "khaki")))
      (insert "\n")
      (marvin-scroll-marvin-frame))))

(defun marvin-remove-prompt-emoji ()
  (save-excursion
    (goto-char (point-min))
    (while (search-forward marvin-log-prompt-emoji nil t)
      (replace-match ""))))

(defun marvin-find-tty-frame ()
  (cl-find-if (lambda (x)
                (eq t (framep x)))
              (frame-list)))

(defun marvin-decorate-output (string)
  (with-temp-buffer
    (insert string)
    (goto-char (point-min))
    (while (re-search-forward marvin-code-begin-regexp nil t)
      (let ((beg (point)))
        (re-search-forward marvin-code-end-regexp nil t)
        (let ((end (point)))
          (org-src-font-lock-fontify-block "emacs-lisp" beg end))))
    (buffer-substring (point-min) (point-max))))

(defvar marvin-elapsed-time-string "")

(defun marvin-log-output (string)
  (unless (marvin-queued-modules-p)
    (setf marvin-currently-loading-module nil)
    (setf marvin-modules-being-loaded nil)
    (setf marvin-module-queue nil)
    (setf marvin-elapsed-time-string
          (concat "Elapsed time: "
                  (format-time-string "%M:%S" (time-subtract (current-time) marvin-start-time))))
    (marvin-click))
  (with-current-buffer (get-buffer-create marvin-log-buffer-name)
    (marvin-remove-prompt-emoji)
    (goto-char (point-max))
    (insert "\n")
    (insert marvin-assistant-emoji)
    (insert " ")
    (insert (propertize (marvin-decorate-output string)
                        'face '(:background "pale turquoise")))
    (insert "\n\n")
    (insert marvin-log-prompt-emoji)
    (insert " ")
    (goto-char (point-max))
    (marvin-scroll-marvin-frame)))

(defvar marvin-log-prompt-emoji "⏩")

(defun marvin-ret ()
  (interactive)
  (setf marvin-start-time (current-time))
  (save-excursion
    (goto-char (point-min))
    (search-forward marvin-log-prompt-emoji nil t)
    (let ((prompt (buffer-substring-no-properties (point) (point-max))))
      (delete-region (point) (point-max))
      (marvin-do prompt)
      (marvin-remove-prompt-emoji)))
  (goto-char (point-max)))

(defun marvin-save-memory (string)
  (marvin-add-note "Memory Dump" :content string))

(setf marvin-alsa-midi-device "/dev/midi5")

(defun marvin-midikbd-open ()
  (interactive)
  (midikbd-open marvin-alsa-midi-device))

(defun marvin-bind-midi-note (midi-note-name thunk)
  (global-set-key (vconcat (make-vector 1 'Ch1)
                           (make-vector 1 midi-note-name))
                  thunk))

(defun marvin-bind-midi-note-to-insertion (midi-note-name)
  (marvin-bind-midi-note midi-note-name
                         (lambda ()
                           (interactive)
                           (marvin-handle-midi-note midi-note-name))))

(defun marvin-handle-midi-note (midi-note-name)
  (marvin-button))

(defvar marvin-midikbd-note-names [C_-1 Csharp_-1 D_-1 Dsharp_-1
  E_-1 F_-1 Fsharp_-1 G_-1 Gsharp_-1 A_-1 Asharp_-1 B_-1 C_0
  Csharp_0 D_0 Dsharp_0 E_0 F_0 Fsharp_0 G_0 Gsharp_0 A_0
  Asharp_0 B_0 C_1 Csharp_1 D_1 Dsharp_1 E_1 F_1 Fsharp_1 G_1
  Gsharp_1 A_1 Asharp_1 B_1 C_2 Csharp_2 D_2 Dsharp_2 E_2 F_2
  Fsharp_2 G_2 Gsharp_2 A_2 Asharp_2 B_2 C_3 Csharp_3 D_3
  Dsharp_3 E_3 F_3 Fsharp_3 G_3 Gsharp_3 A_3 Asharp_3 B_3 C_4
  Csharp_4 D_4 Dsharp_4 E_4 F_4 Fsharp_4 G_4 Gsharp_4 A_4
  Asharp_4 B_4 C_5 Csharp_5 D_5 Dsharp_5 E_5 F_5 Fsharp_5 G_5
  Gsharp_5 A_5 Asharp_5 B_5 C_6 Csharp_6 D_6 Dsharp_6 E_6 F_6
  Fsharp_6 G_6 Gsharp_6 A_6 Asharp_6 B_6 C_7 Csharp_7 D_7
  Dsharp_7 E_7 F_7 Fsharp_7 G_7 Gsharp_7 A_7 Asharp_7 B_7 C_8
  Csharp_8 D_8 Dsharp_8 E_8 F_8 Fsharp_8 G_8 Gsharp_8 A_8
  Asharp_8 B_8 C_9 Csharp_9 D_9 Dsharp_9 E_9 F_9 Fsharp_9 G_9])

(defun marvin-bind-all-midi-notes ()
  (interactive)
  (cl-loop for i from 0 to 127 do
        (marvin-bind-midi-note-to-insertion
         (aref marvin-midikbd-note-names i))))

(defun marvin-enable-midi ()
  "Turn on MIDI push-to-talk support."
  (interactive)
  (marvin-midikbd-open)
  (marvin-bind-all-midi-notes))

(defvar marvin-button-state nil)

(defun marvin-reset-button-state ()
  (setf marvin-button-state nil))

(defun marvin-button ()
  (marvin-click))

(defun marvin-scroll-marvin-frame ()
  (interactive)
  (let ((original (selected-frame))
        (marvin (marvin-find-tty-frame)))
    (cl-dolist (frame (frame-list))
      (when (eq frame marvin)
        (save-window-excursion
          (select-frame marvin)
          (switch-to-buffer marvin-log-buffer-name)
          (goto-char (point-max))
          (ding)
          (recenter-top-bottom)
          (recenter-top-bottom)
          (recenter-top-bottom)
          )
        (redisplay t)))
    (select-frame original)))

(defun marvin-close ()
  (interactive)
  (setf marvin-currently-loading-module nil)
  (setf marvin-modules-being-loaded nil)
  (setf marvin-module-queue nil)
  (marvin-quit-hy)
  (marvin-stop-timer))

(defun marvin-check-signature ()
  (interactive)
  (marvin-do (format "(print (string-compare marvin-personality-signature %S))"
                     marvin-personality-signature)))

(setf marvin-use-voice-synthesis nil)

(setf marvin-code-begin-regexp "^\`\`\`\\([A-Za-z]+\\)\n")
(setf marvin-code-end-regexp "^\`\`\`")

(defun marvin-load-module (file)
  (interactive "fMarvinLisp module to load: ")
  (marvin-queue-modules (list (file-name-nondirectory file))))

(defun marvin-load-string (string)
  (marvin-queue-modules (list (vector string))))

(defun marvin-tell-date ()
  (interactive)
  (marvin-do (format "(setf marvin-date %S)\n"
                     (format-time-string "%Y-%m-%d"))))

(defcustom marvin-disable-eval-in-emacs t
  "When non-nil, disable calling Emacs Lisp functions on behalf of
assistant."
  :type 'boolean
  :group 'marvin)

(defun marvin-startup (prefix)
  (setf marvin-currently-loading-module nil)
  (setf marvin-modules-being-loaded nil)
  (setf marvin-module-queue nil)
  (setf marvin-start-time (current-time))
  (unless prefix
    (marvin-queue-modules
     (list "marvin.mrv"
           "asimov.mrv"
           "emacs.mrv"
           ;; "posix.mrv"
           ;; "plan.mrv"
           (vector (marvin-expand-all-learning-templates))
           "intro.mrv"))))

(defun marvin-create (model-class &optional prefix)
  (setf *marvin* (make-instance model-class))
  (marvin-startup prefix))
  
(defun marvin-mistral-instruct (prefix)
  (interactive "P")
  (marvin-create marvin-mistral-instruct-7b prefix)
  (marvin-begin-session (marvin-self)))

(defun marvin-mistral-code (prefix)
  (interactive "P")
  (marvin-create marvin-mistral-code-7b prefix)
  (marvin-begin-session (marvin-self)))

(defun marvin-mistral-lite (prefix)
  (interactive "P")
  (marvin-create marvin-mistral-lite-7b prefix)
  (marvin-begin-session (marvin-self)))

(defun marvin-rift (prefix)
  (interactive "P")
  (marvin-create marvin-rift prefix)
  (marvin-begin-session (marvin-self)))

(defun marvin-falcon (prefix)
  (interactive "P")
  (marvin-create marvin-falcon-7b prefix)
  (marvin-begin-session (marvin-self)))

(defvar marvin-start-time nil)

(defcustom marvin-use-automatic-temperature-control t
  "When non-nil, manage model temperature automatically.
This is highly recommended."
  :type 'number
  :group 'marvin)

(defvar marvin-learning-templates ())
(setf marvin-learning-templates
     `(("(eval-in-emacs marvin-beep)"
        "<<<(marvin-beep)>>>")
       ("Make emacs beep."
        "<<<(marvin-beep)>>>")
       ("Please beep."
       "<<<(marvin-beep)>>>")
       ("(beep)"
       "<<<(marvin-beep)>>>")
       ("Take a note of <<content>>"
       "<<<(marvin-add-note <<content>>)>>>")
       ("Save <<content>> as a note."
       "<<<(marvin-add-note <<content>>)>>>")
       ("Save <<content>> to Emacs."
       "<<<(marvin-add-note <<content>>)>>>")
       ("(eval-in-emacs marvin-add-note <<content>>)"
        "<<<(marvin-add-note <<content>>)>>>")))

(cl-defmethod marvin-expand-learning-template ((self marvin) template)
  (cl-destructuring-bind (input output) template
    (format (marvin-learning-template self)
            input output)))

(defun marvin-expand-all-learning-templates ()
  (with-temp-buffer
    (dolist (template marvin-learning-templates)
      (insert (marvin-expand-learning-template (marvin-self) template)))
    (buffer-substring-no-properties (point-min) (point-max))))

(defun marvin-alpha ()
  (interactive)
  (marvin-create marvin-alpha-7b t)
  (marvin-begin-session (marvin-self)))

(defun marvin (prefix)
  (interactive "P")
  (marvin-alpha))

;; (defun marvin-insinuate-font-lock ()
;;   (interactive)
;;   (font-lock-add-keywords nil   
;;                           '(("\\[RESULT\\]" . font-lock-function-name-face prepend)
;;                             ("\\[TRACE\\]" . font-lock-function-name-face)
;;                             ("\\[STATUS\\]" . font-lock-function-name-face))))

(provide 'marvin)
;;; marvin.el ends here
