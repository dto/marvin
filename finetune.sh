cd ~/src/alpaca-lora

HSA_OVERRIDE_GFX_VERSION=10.3.0 python finetune-dto.py \
    --base_model '/home/dto/src/alpaca-lora/bling-falcon-1b-0.1' \
    --data_path '/home/dto/ai/dataset.json' \
    --output_dir './lora-alpaca' \
    --batch_size 128 \
    --micro_batch_size 4 \
    --num_epochs 3 \
    --learning_rate 1e-4 \
    --cutoff_len 512 \
    --val_set_size 2000 \
    --lora_r 8 \
    --lora_alpha 16 \
    --lora_dropout 0.05 \
    --lora_target_modules '[query_key_value]' \
    --train_on_inputs \
    --group_by_length

BASE_MODEL="/home/dto/src/alpaca-lora/bling-falcon-1b-0.1" python export_hf_checkpoint-dto.py 
cp bling-falcon-1b-0.1/*.json hf_ckpt
rm -rf marvin-bling
mv hf_ckpt marvin-bling

